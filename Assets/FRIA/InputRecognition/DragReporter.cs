﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragReporter : MonoBehaviour
{
    const float defWidth = 900;
    public float defPixCount = 15;
    public event System.Action<Vector2> onDrag_Total;
    public event System.Action<Vector2> onDrag_Incremental;
    public event System.Action<Vector3> onTap;
    float minimalPixelCount;

    public static DragReporter instance;
    public Drag currentDrag = null;
    private void Start()
    {
        instance = this;
        minimalPixelCount =  (defPixCount/defWidth)* Screen.width;
    }
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            currentDrag = new Drag(new Vector2(Input.mousePosition.x, Input.mousePosition.y), minimalPixelCount);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            if (currentDrag != null && currentDrag.didntDragFarEnough)
            {
                onTap?.Invoke(Input.mousePosition);
            }
            currentDrag = null;
        }

        if (currentDrag != null)
        {
            currentDrag.UpdateDragCurrentPos(new Vector2(Input.mousePosition.x, Input.mousePosition.y));
            //Vector2 v = currentDrag.GetTotalDrag();
            //Vector2 resMult = new Vector2(900.0f/Screen.width , 1800.0f/Screen.height );
            //Debug.Log(v*resMult);
            onDrag_Total?.Invoke(currentDrag.GetTotalDrag());
            onDrag_Incremental?.Invoke(currentDrag.GetIncrementalDrag());
        }
    }
}

public class Drag
{
    Vector2 startPos;
    Vector2 prevPos;
    Vector2 lastPos;
    bool dragConfirmed;
    public Vector2 travelVec { get { return lastPos - startPos; } }
    //float startTime;
    float minPixel = 100;

    public bool didntDragFarEnough
    {
        get
        {
            return travelVec.magnitude < minPixel;
        }
    }
    public Drag(Vector2 startPosition, float minPix)
    {
        this.minPixel = minPix;
        //startTime = Time.time;
        startPos = startPosition;
        prevPos = startPosition;
        lastPos = startPosition;
        dragConfirmed = false;
    }

    public void UpdateDragCurrentPos(Vector2 currentPosition)
    {
        prevPos = lastPos;
        lastPos = currentPosition;
    }
    public Vector2 GetTotalDrag()
    {
        if (!dragConfirmed && didntDragFarEnough)
        {
            return Vector2.zero;
        }
        else
        {
            dragConfirmed = true;
            return lastPos-startPos;
        }
    }
    public Vector2 GetIncrementalDrag()
    {
        if (!dragConfirmed && didntDragFarEnough)
        {
            return Vector2.zero;
        }
        else
        {
            dragConfirmed = true;
            return lastPos-prevPos;
        }
    }
}
