﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapPanController : MonoBehaviour
{
    public DragReporter dragReporter;
    public Transform dragTarget;
    public float dragMult = 0.001f;
    public Vector2 positionRange;
    public float maxSpeed;
    public float dampRate = 10;

    
    // Start is called before the first frame update
    public void Focus(Transform tr)
    {
        //Debug.Log(tr.name);
        dragTarget.position = new Vector3(tr.position.x, dragTarget.position.y, tr.position.z);
        ClampPos();
    }

    Vector2 resMult;

    void Start()
    {
        resMult = new Vector2(900.0f / Screen.width, 1800.0f / Screen.height);
        dragReporter.onDrag_Incremental += onDrag;

        velocity = Vector2.zero;
    }

    Vector2 velocity;
    private void onDrag(Vector2 dragAmount)
    {
        dragAmount = dragAmount * resMult;
        velocity += dragAmount * dragMult;
        float m = velocity.magnitude;
        if (m != 0)
            velocity = velocity * Mathf.Clamp(m, 0, maxSpeed) / m;


    }
    private void LateUpdate()
    {
        velocity = Vector2.Lerp(velocity, Vector2.zero, 10.0f * Time.deltaTime);// * Mathf.Clamp(m, 0, maxSpeed) / m;

        float x = dragTarget.localPosition.x + velocity.x;
        float y = dragTarget.localPosition.z + velocity.y;
        dragTarget.localPosition = new Vector3(x, dragTarget.localPosition.y, y);
        ClampPos();
    }
    private void ClampPos()
    {
        float x = dragTarget.localPosition.x;
        float y = dragTarget.localPosition.z;
        x = Mathf.Clamp(x, -positionRange.x, positionRange.x);
        y = Mathf.Clamp(y, -positionRange.y, positionRange.y);
        dragTarget.localPosition = new Vector3(x, dragTarget.localPosition.y, y);
    }
}
