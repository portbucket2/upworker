﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "FRIA/VertexColorControl/VColMapped" {
   Properties {
      _Color0 ("BaseColor", Color) = (1,1,1,1) 

      _Color1 ("R_Color", Color) = (1,1,1,1) 
      _Color2 ("G_Color", Color) = (1,1,1,1) 
      _Color3 ("B_Color", Color) = (1,1,1,1) 
      _Color4 ("A_Color", Color) = (1,1,1,1) 
	  _BumpMap("Normal Map", 2D) = "bump" {}
   }
   SubShader {
      Pass {	
        // Tags { "Queue"="Opaque"  "RenderType"="Transparent" "LightMode" = "ForwardBase" } 
            // pass for ambient light and first light source
			   ZWrite On
        //Blend SrcAlpha OneMinusSrcAlpha
		 Cull Off
         CGPROGRAM
 
         #pragma vertex vert  
         #pragma fragment frag 
 
         #include "UnityCG.cginc"
         uniform float4 _LightColor0; 
            // color of light source (from "Lighting.cginc")
 
         // User-specified properties
         uniform float4 _Color0;
         uniform float4 _Color1;
         uniform float4 _Color2;
         uniform float4 _Color3;
         uniform float4 _Color4;
         uniform  sampler2D _BumpMap; 
		 uniform float4 _BumpMap_ST;		 

         struct vertexInput {
			float4 color : COLOR;
            float4 vertex : POSITION;
            float3 normal : NORMAL;
			float4 tangent : TANGENT;
			float2 uv : TEXCOORD0;
         };

         struct vertexOutput {
            float4 pos : SV_POSITION;
			float2 uv : TEXCOORD0;
            float4 posWorld : TEXCOORD1;
			float4 vertColor : TEXCOORD2;
			float3 T : TEXCOORD3;
			float3 B : TEXCOORD4;
			float3 N : TEXCOORD5;
         };
 
         vertexOutput vert(vertexInput input) 
         {
            vertexOutput output;
 
            float4x4 modelMatrix = unity_ObjectToWorld;
            float4x4 modelMatrixInverse = unity_WorldToObject; 
			output.T = normalize(mul(modelMatrix, float4(input.tangent.xyz, 0.0)).xyz);
            output.N = normalize(mul(float4(input.normal, 0.0), modelMatrixInverse).xyz);
            output.B = normalize(cross(output.N, output.T) * input.tangent.w); // tangent.w is specific to Unity
            output.posWorld = mul(modelMatrix, input.vertex);
			output.uv = input.uv.xy * _BumpMap_ST.xy + _BumpMap_ST.zw;
            output.pos = UnityObjectToClipPos(input.vertex);
			output.vertColor = input.color;
            return output;
         }
 
         float4 frag(vertexOutput input) : COLOR
         {
		 /*
			float3 tangentNormal = tex2D(_BumpMap, input.uv).xyz;
			// and change range of values (0 ~ 1)
			tangentNormal = normalize(tangentNormal * 2 - 1);

			// 'TBN' transforms the world space into a tangent space
			// we need its inverse matrix
			// Tip : An inverse matrix of orthogonal matrix is its transpose matrix
			float3x3 TBN = float3x3(normalize(input.T), normalize(input.B), normalize(input.N));
			TBN = transpose(TBN);

			// finally we got a normal vector from the normal map
			float3 normalDirection = mul(TBN, tangentNormal);
			*/


			//###
			float4 encodedNormal = tex2D(_BumpMap, _BumpMap_ST.xy * input.uv.xy + _BumpMap_ST.zw);
			float3 localCoords = 2.0 * encodedNormal.rgb - float3(1.0, 1.0, 1.0);
			float3x3 local2WorldTranspose = float3x3(input.T, input.B, input.N);
			float3 normalDirection = normalize(mul(localCoords, local2WorldTranspose));

			//###

            float3 viewDirection = normalize(
               _WorldSpaceCameraPos - input.posWorld.xyz);
            float3 lightDirection;
            float attenuation;
 
            if (0.0 == _WorldSpaceLightPos0.w) // directional light?
            {
               attenuation = 1.0; // no attenuation
               lightDirection = normalize(_WorldSpaceLightPos0.xyz);
            } 
            else // point or spot light
            {
               float3 vertexToLightSource = 
                  _WorldSpaceLightPos0.xyz - input.posWorld.xyz;
               float distance = length(vertexToLightSource);
               attenuation = 1.0 / distance; // linear attenuation 
               lightDirection = normalize(vertexToLightSource);
            }
 
            float3 ambientLighting =  UNITY_LIGHTMODEL_AMBIENT.rgb ;
            float3 diffuseReflection = attenuation * _LightColor0.rgb * max(0.0, dot(normalDirection, lightDirection));
			float totalVCol  = input.vertColor.r + input.vertColor.g + input.vertColor.b + input.vertColor.a;
			float4 choiceColor = (input.vertColor.r*_Color1 + input.vertColor.g*_Color2 + input.vertColor.b*_Color3 + input.vertColor.a*_Color4)/(totalVCol+0.0001);
			float lerpVal = clamp(totalVCol,0,1);
			choiceColor =  lerp(_Color0,choiceColor,lerpVal);
			choiceColor.a =1;
			float4 finalCol = float4(ambientLighting + diffuseReflection 
               //+ specularReflection
			   , 1.0)* choiceColor;
            return  finalCol;
         }
 
         ENDCG
      }
   }
   Fallback "Specular"
}