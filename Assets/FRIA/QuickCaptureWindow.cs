﻿using UnityEngine;
using FRIA;
using System.Collections.Generic;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
public class QuickCaptureWindow : EditorWindow
{
    const string CAPTURE_ROOT = "Capture";
    const string CAPTURE_ROOT_CAMERA = "Camera";
    const string CAPTURE_ROOT_SCREEN = "Screen";

    string filename;
    int pixWidth = 512;
    int pixHeight = 512;
    HardData<int> countHD;
    int count
    {
        get
        {
            if (countHD == null)
            {
                countHD = new HardData<int>("FRIA_SCREEN_CAPTURE_COUNT", 0);
            }
            return countHD.value;
        }
        set
        {
            if (countHD == null)
            {
                countHD = new HardData<int>("FRIA_SCREEN_CAPTURE_COUNT", 0);
            }
            countHD.value = value;
        }
    }

    [MenuItem("FRIA/Windows/Quick Capture Window")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        QuickCaptureWindow window = (QuickCaptureWindow)EditorWindow.GetWindow(typeof(QuickCaptureWindow));
        window.pixHeight = 768;
        window.pixWidth = 1024;
        window.Show();

    }

    void Allign()
    {
        SceneView sceneView = SceneView.lastActiveSceneView;
        Camera cam = sceneView.camera;
        Camera.main.transform.position = cam.transform.position;
        Camera.main.transform.rotation = cam.transform.rotation;
    }
    void Capture()
    {
        Camera camera = Camera.main;

        camera.targetTexture = new RenderTexture(pixWidth,pixHeight,0,RenderTextureFormat.Default);
        var currentRT = RenderTexture.active;
        RenderTexture.active = camera.targetTexture;

        // Render the camera's view.
        camera.Render();

        // Make a new texture and read the active Render Texture into it.
        Texture2D image = new Texture2D(camera.targetTexture.width, camera.targetTexture.height);
        image.ReadPixels(new Rect(0, 0, camera.targetTexture.width, camera.targetTexture.height), 0, 0);
        image.Apply();

        // Replace the original active Render Texture.
        RenderTexture.active = currentRT;
        camera.targetTexture = null;

        byte[] bytes = image.EncodeToPNG();
        File.WriteAllBytes(string.Format("{0}{1}.png", filename, count++),bytes);
        //File.WriteAllBytes(string.Format("LevelPhoto_{0}_{1}.png", LevelManager.SelectedLevel.id, count++), bytes);
    }

    public void ScreenShot()
    {
        ScreenCapture.CaptureScreenshot(string.Format("{0}{1}.png", filename, count++));
    }
    
    void OnGUI()
    {
        GUILayout.Space(3);

        string[] guids = AssetDatabase.FindAssets("QuickCaptureWindow t:Script");
        string path = AssetDatabase.GUIDToAssetPath(guids[0]);
        Object obj = AssetDatabase.LoadAssetAtPath<Object>(path);
        EditorGUI.BeginDisabledGroup(true);
        EditorGUILayout.ObjectField("Script:", obj, typeof(Object), false);
        EditorGUI.EndDisabledGroup();

       // EditorGUILayout.BeginVertical("Camera Capture");
        filename = EditorGUILayout.TextField("FileHeader", filename);
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Dimention");
        pixWidth = EditorGUILayout.IntField(pixWidth);
        pixHeight = EditorGUILayout.IntField(pixHeight);
        EditorGUILayout.EndHorizontal();
        GUILayout.Space(5);
        if (GUILayout.Button("Allign Camera", GUILayout.Height(35)))
        {
            Allign();
        }
        if (GUILayout.Button("Capture Camera", GUILayout.Height(35)))
        {
            Capture();
        }
        //EditorGUILayout.EndVertical();
        GUILayout.Space(50);
        //EditorGUILayout.BeginVertical("Screen Capture");
        if (GUILayout.Button("Capture Screen", GUILayout.Height(35)))
        {
            ScreenShot();
        }
        //EditorGUILayout.EndVertical();
    }
  

}
#endif