﻿using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
public class RigTransferWindow : EditorWindow
{
    //Material mat;

    GameObject sourceObject;
    GameObject targetObject;

    string meshRootName;
    string boneRootName;
    // Add menu named "My Window" to the Window menu
    [MenuItem("FRIA/Windows/Rig Transfer Window")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        RigTransferWindow window = (RigTransferWindow)EditorWindow.GetWindow(typeof(RigTransferWindow));
        window.Show();
        window.Refresh();
       

    }
    void Refresh()
    {
        meshRootName = "Mesh_Char01";
        boneRootName = "Bind_Hips";
        targetObject = GameObject.Find("Rig 3");
        GameObject badBone = GameObject.Find("final_joint27");
        if (badBone)
        {
            badBone.name = "joint27";
        }
    }

    void OnGUI()
    {
        GUILayout.Space(3);

        string[] guids = AssetDatabase.FindAssets("RigTransferWindow t:Script");
        string path = AssetDatabase.GUIDToAssetPath(guids[0]);
        Object obj = AssetDatabase.LoadAssetAtPath<Object>(path);
        EditorGUI.BeginDisabledGroup(true);
        EditorGUILayout.ObjectField("Script:", obj, typeof(Object), false);
        EditorGUI.EndDisabledGroup();

        GUILayout.Space(5);
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Items", EditorStyles.boldLabel);
        if (GUILayout.Button("Refresh", GUILayout.Height(35)))
        {
            Refresh();
        }
        EditorGUILayout.EndHorizontal();
        sourceObject = (GameObject)EditorGUILayout.ObjectField("Source", sourceObject, typeof(GameObject), true);
        targetObject = (GameObject)EditorGUILayout.ObjectField("Target", targetObject, typeof(GameObject), true);
        GUILayout.Space(5);
        EditorGUILayout.LabelField("Conventions", EditorStyles.boldLabel);
        boneRootName = EditorGUILayout.TextField("Bone Root", boneRootName);
        meshRootName = EditorGUILayout.TextField("Mesh Root", meshRootName);

        GUILayout.Space(5);

        if (GUILayout.Button("Transfer Mesh Data", GUILayout.Height(35)))
        {
            MeshTransfer();
        }
    }
    int successfulBones = 0;
    int failedBones = 0;
    Dictionary<Transform, Transform> boneDic = new Dictionary<Transform, Transform>();

    void BoneTransfer()
    {
        Debug.Log("Transfering Bone data!");
        successfulBones = 0;
        failedBones = 0;
        boneDic.Clear();
        BoneCopy(sourceObject.transform.Find(boneRootName), targetObject.transform.Find(boneRootName));
        Debug.LogFormat("Process End, Success: {0}, Fail: {1}", successfulBones, failedBones);
    }

    void BoneCopy(Transform source, Transform target)
    {
        boneDic.Add(source, target);
        successfulBones++;
        target.localPosition = source.localPosition;
        target.localRotation = source.localRotation;
        target.localScale = source.localScale;

        int nT = target.childCount;
        int nS = source.childCount;
        if (nS != nT)
        {
            Debug.LogError("Child count mismatch!");
        }
        foreach (Transform subSource in source)
        {
            Transform subTarget = target.Find(subSource.name);
            if (!subTarget)
            {

                boneDic.Add(subSource, null);
                failedBones++;
                Transform transform = subSource;
                string path = transform.name;
                while (transform.parent != null)
                {
                    transform = transform.parent;
                    path = transform.name + "/" + path;

                }
                Debug.LogErrorFormat("Equivalent bone not found for: {0}", path);

                //subTarget = new GameObject(subSource.name).transform;
                //subTarget.SetParent(target);
                //BoneCopy(subSource, subTarget);
            }
            else
            {
                BoneCopy(subSource, subTarget);
            }
        }
        foreach (Transform subSource in source)
        {
            boneDic[subSource].SetAsLastSibling();
        }

        EditorFix.SetObjectDirty(target);
    }

    void MeshTransfer()
    {
        BoneTransfer();
        Debug.Log("Transfering Mesh data!");
        foreach (Transform source in sourceObject.transform.Find(meshRootName))
        {
            Transform target = targetObject.transform.Find(meshRootName).Find(source.name);
            if (!target)
            {
                Transform transform = source;
                string path = transform.name;
                while (transform.parent != null)
                {
                    transform = transform.parent;
                    path = transform.name + "/" + path;

                }
                Debug.LogErrorFormat("Equivalent mesh not found for: {0}", path);
            }
            else
            {
                MeshCopy(source,target);
            }
        }
    }
    void MeshCopy(Transform source, Transform target)
    {
        SkinnedMeshRenderer skinSource = source.GetComponent<SkinnedMeshRenderer>();
        SkinnedMeshRenderer skinTarget= target.GetComponent<SkinnedMeshRenderer>();
        if (skinSource && skinTarget)
        {
            target.localPosition = source.localPosition;
            target.localRotation = source.localRotation;
            target.localScale = source.localScale;
            EditorFix.SetObjectDirty(target);

            skinTarget.sharedMesh = skinSource.sharedMesh;
            skinTarget.rootBone = boneDic[skinSource.rootBone];
            skinTarget.localBounds = skinSource.localBounds;
            skinTarget.sharedMaterials = skinSource.sharedMaterials;
            Transform[] sourceBones = skinSource.bones;
            Transform[] targetBones = new Transform[sourceBones.Length];
            for (int i = 0; i < targetBones.Length; i++)
            {
                targetBones[i] = boneDic[sourceBones[i]];
            }
            skinTarget.bones = targetBones;
            skinTarget.skinnedMotionVectors = true;
            EditorFix.SetObjectDirty(skinTarget);
        }
        else
        {
            Debug.LogError("Skin not found");
        }
    }

}
//[System.Serializable]
//public class BrokenBoneLink
//{
//    public string sourceName;
//    public string targetName;
//}
#endif