﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestABController : MonoBehaviour
{
    public GameObject root;

    public Button applyButton;
    // Start is called before the first frame update
    public static bool isAB_trialValueSetupPending = false;
    IEnumerator Start()
    {
        root.SetActive(false);
        if (SpecialBuildManager.enableABTestUI && modules.Count>0)
        {
            isAB_trialValueSetupPending = true;
            while (!ABManager.isDataFetchComplete)
            {
                yield return null;
            }
            root.SetActive(true);
            LoadModules();
            applyButton.onClick.AddListener(OnApply);
            for (int i = modules.Count -1 ; i >=0; i--)
            {
                if (!modules[i].loaded)
                {
                    modules[i].gameObject.SetActive(false);
                    modules.RemoveAt(i);
                }
            }
            if (modules.Count == 0) OnApply();
        }

    }
    public List<TestABDataChangeUI> modules;
    //public TestABDataChangeUI module0;
    //public TestABDataChangeUI reviveModule;
    void LoadModules()
    {
        LoadModuleWith_AdFreq(0);
        LoadModuleWith_CollectibleEnabling(1);
        LoadModuleWith_BonusCharachterEnabling(2);
        //if(BuildSceneInfo.Instance.sequenceMode == SequenceMode.AB) LoadModuleWith_LevelSequence(0);
        //LoadModuleWith_StepCountChoice(0);
        //LoadModuleWith_AdIntSuccess(1);
        //LoadModuleWith_AdIntFail(2);
        //homecomingModule.Load(ABtype.home_return_freq, new string[] {"0","1","2","3","4","5"} , 
        //    (int selectionIndex)=> 
        //    {
        //        switch (selectionIndex)
        //        {
        //            default:
        //                return string.Format("every {0} level",selectionIndex);
        //            case 0:
        //                return string.Format("disable auto-return");
        //            case 1:
        //                return string.Format("every level");
        //        }
        //    });
        //reviveModule.Load(ABtype.coin_revive_enable, new string[] { "0", "1"},
        //    (int selectionIndex) =>
        //    {
        //        switch (selectionIndex)
        //        {
        //            default:
        //            case 1:
        //                return string.Format("enabled");
        //            case 0:
        //                return string.Format("disabled");
        //        }
        //    });
    }
    void LoadModuleWith_BonusCharachterEnabling(int i)
    {
//#if UNITY_ANDROID
        modules[i].Load(ABtype.bonus_characters, new string[] { "0", "1" },
            (int selectionIndex) =>
            {
                switch (selectionIndex)
                {
                    default:
                        return string.Format("status {0}", selectionIndex);
                    case 0:
                        return string.Format("disabled");
                    case 1:
                        return string.Format("enabled");

                }
            });
//#endif
    }
    void LoadModuleWith_CollectibleEnabling(int i)
    {
#if UNITY_ANDROID
        modules[i].Load(ABtype.enable_collectibles, new string[] { "0", "1" },
            (int selectionIndex) =>
            {
                switch (selectionIndex)
                {
                    default:
                        return string.Format("status {0}", selectionIndex);
                    case 0:
                        return string.Format("disabled");
                    case 1:
                        return string.Format("enabled");

                }
            });
#endif
    }
    void LoadModuleWith_AdFreq(int i)
    {
#if UNITY_IOS
        modules[i].Load(ABtype.interstitial_disable, new string[] { "0", "1"},
            (int selectionIndex) =>
            {
                switch (selectionIndex)
                {
                    default:
                        return string.Format("{0} = unknown", selectionIndex);
                    case 0:
                        return string.Format("false");
                    case 1:
                        return string.Format("true");

                }
            });
#endif
    }

    //void LoadModuleWith_LevelSequence(int i)
    //{
    //    modules[i].Load(ABtype.level_sequence, new string[] { "0", "1", "2" },
    //        (int selectionIndex) =>
    //        {
    //            switch (selectionIndex)
    //            {
    //                default:
    //                    return string.Format("sequence {0}", selectionIndex);
    //                case 0:
    //                    return string.Format("control group");
    //                case 1:
    //                    return string.Format("Arrangement 1");
    //                case 2:
    //                    return string.Format("Arrangement 2");

    //            }
    //        });
    //}
    //void LoadModuleWith_StepCountChoice(int i)
    //{
    //    modules[i].Load(ABtype.step_count_choice, new string[] { "0", "1" },
    //        (int selectionIndex) =>
    //        {
    //            switch (selectionIndex)
    //            {
    //                default:
    //                    return string.Format("step choice : {0}", selectionIndex);
    //                case 0:
    //                    return string.Format("2 step");
    //                case 1:
    //                    return string.Format("3 step");

    //            }
    //        });
    //}
    //void LoadModuleWith_AdIntSuccess(int i)
    //{
    //    modules[i].Load(ABtype.inter_point, new string[] { "0", "1"},
    //        (int selectionIndex) =>
    //        {
    //            switch (selectionIndex)
    //            {
    //                default:
    //                    return string.Format("{0}", selectionIndex);
    //                case 0:
    //                    return string.Format("Pre UI");
    //                case 1:
    //                    return string.Format("Post UI");

    //            }
    //        });
    //}
    //void LoadModuleWith_AdIntFail(int i)
    //{
    //    modules[i].Load(ABtype.fail_ad_pos, new string[] { "0", "1" },
    //        (int selectionIndex) =>
    //        {
    //            switch (selectionIndex)
    //            {
    //                default:
    //                    return string.Format("{0}", selectionIndex);
    //                case 0:
    //                    return string.Format("Post UI");
    //                case 1:
    //                    return string.Format("Pre UI");

    //            }
    //        });
    //}


    void OnApply()
    {
        foreach (var item in modules)
        {
            if(item.loaded) item.OnApply();
        }
        root.SetActive(false);
        isAB_trialValueSetupPending = false;
    }

}
