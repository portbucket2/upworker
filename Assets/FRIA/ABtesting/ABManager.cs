﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public enum ABtype
{
    //gameplay_type = 0,
    //ad_frequency = 1,
    //home_return_freq = 2,
    //coin_revive_enable = 3,
    //level_sequence = 4,
    //inter_point = 5,
    //fail_ad_pos = 6,
    //step_count_choice = 7,
    //rv_choice_enable = 8,
#if UNITY_IOS
    interstitial_disable = 9,
#endif

#if UNITY_ANDROID
    enable_collectibles = 10,
#endif
    bonus_characters = 11,
}
public static class ABManager
{
    public static bool isDataFetchComplete { get; private set; }
    public static void SetFetchComplete() { isDataFetchComplete = true; }
    private static void Init()
    {
        _allSettings = new Dictionary<ABtype, HardAB>();
        //_allSettings.Add(ABtype.gameplay_type, new HardAB(ABtype.gameplay_type,"gameplay_type"));
#if UNITY_IOS
        _allSettings.Add(ABtype.interstitial_disable, new HardAB(ABtype.interstitial_disable, "inter_disable", isVolatile: true));
#endif
        //_allSettings.Add(ABtype.home_return_freq, new HardAB(ABtype.home_return_freq, "return_home"));
        //_allSettings.Add(ABtype.coin_revive_enable, new HardAB(ABtype.coin_revive_enable, "coins_revive"));
        //_allSettings.Add(ABtype.level_sequence, new HardAB(ABtype.level_sequence, "level_arrange"));
        //_allSettings.Add(ABtype.inter_point, new HardAB(ABtype.inter_point, "inter_point"));
        //_allSettings.Add(ABtype.fail_ad_pos, new HardAB(ABtype.fail_ad_pos, "fail_placement"));
        //_allSettings.Add(ABtype.step_count_choice, new HardAB(ABtype.step_count_choice, "three_step")); 
        //_allSettings.Add(ABtype.rv_choice_enable, new HardAB(ABtype.rv_choice_enable, "third_option"));

#if UNITY_ANDROID
        _allSettings.Add(ABtype.enable_collectibles, new HardAB(ABtype.enable_collectibles, "prank_tool"));
#endif
        _allSettings.Add(ABtype.bonus_characters, new HardAB(ABtype.bonus_characters, "bonus_characters"));
    }

    private static Dictionary<ABtype, HardAB> _allSettings;
    public static Dictionary<ABtype, HardAB> allSettings
    {
        get
        {
            if (_allSettings == null) Init();
            return _allSettings;
        }
    }

    public static string GetValueString(ABtype type)
    {
        return allSettings[type].GetValue();
    }

    public static int GetValueInt(ABtype type)
    {
        var s = allSettings[type].GetValue();
        int result = 0;
        int.TryParse(s, out result);
        if (result < 0) result = 0;
        return result;
    }

    public static float GetValueFloat(ABtype type)
    {
        var s = allSettings[type].GetValue();
        float result = 0f;
        float.TryParse(s, out result);
        return result;
    }

    public static long GetValueLong(ABtype type)
    {
        var s = allSettings[type].GetValue();
        long result = 0;
        long.TryParse(s, out result);
        return result;
    }

    public static bool GetValueBool(ABtype type)
    {
        var s = allSettings[type].GetValue();
        bool result = false;
        bool.TryParse(s, out result);
        return result;
    }


    public static HardAB GetABAccess(ABtype type)
    {
        return allSettings[type];
    }


    public class HardAB
    {
        ABtype type;
        string id;
        string key;
        HardData<string> data;
        bool isVolatile;

        public HardAB(ABtype type, string id, string editorDefaultValue = "", bool isVolatile = false)
        {
            this.isVolatile = isVolatile;
            this.type = type;
            this.id = id;
            key = string.Format("AB_KEY_{0}", id);
#if UNITY_EDITOR
            data = new HardData<string>(key, editorDefaultValue);
#else
            data = new HardData<string>(key, "");
#endif
        }

        public string GetID()
        {
            return id;
        }
        public void Assign_IfUnassigned(string abStringValue)
        {

            if (data.value == "" || isVolatile)
            {
                //switch (type)
                //{
                //    case ABtype.ad_frequency:
                //        Debug.LogError(string.Format("{0} value set: {1} from {2}\n", id, abStringValue,data.value));
                //        break;
                //}
                data.value = abStringValue;
#if ANALYTICS_DEFCHECK
                if (data.value != "") AnalyticsController.LogABTesting(id, data.value);
#endif
            }
            else
            {
                //switch (type)
                //{
                //    case ABtype.ad_frequency:
                //        SpecialBuildManager.Log1(string.Format("{0} value retrieved: {1}\n", id, abStringValue));
                //        break;
                //}
            }
        }

        public void ForceSetValue(string newValue)
        {
            data.value = newValue;

        }
        public string GetValue()
        {
            if (data.value == "")
            {
                //AnalyticsController.LogEventABTesting(id, data.value);
                // GameUtil.LogYellow("AB value was set to default for key " + id, true);
            }
            return data.value;
        }
    }
}
