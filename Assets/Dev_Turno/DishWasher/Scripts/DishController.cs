﻿
using UnityEngine;
using System.Collections;
using DG.Tweening;

public class DishController : MonoBehaviour
{
    public Camera cam;
    public Transform dishChld;
    public Texture2D copyTexture;
    public Texture2D mappingTexture;
    public int counter = 0;
    private Color[] CleanTexColors;

    private Color[] DirtyTexColors;

    bool letStop;
    bool[] progArray;
    public int maxCount, currentCount;
    public float targetRatio = 0.8f;

    public float[] sittingPos;
    float zValue;
    private bool startSeqComp;
    private int index;

    


    void Start()
    {
        cam = Camera.main;

        dishChld = this.gameObject.transform.GetChild(0);
        MeshRenderer mesh = dishChld.GetComponent<MeshRenderer>();
        Texture2D originalTex = mesh.material.mainTexture as Texture2D;
        copyTexture = new Texture2D(originalTex.width, originalTex.height);
        copyTexture.SetPixels(originalTex.GetPixels());
        copyTexture.Apply(false);
        mesh.material.mainTexture = copyTexture;
        CleanTexColors = mappingTexture.GetPixels();
        DirtyTexColors = copyTexture.GetPixels();
        progArray = new bool[DirtyTexColors.Length];
        maxCount = (int)(targetRatio * DirtyTexColors.Length);
        letStop = false;
        //endSeq = false;
        startSeqComp = false;
        StartCoroutine(StartSequence());

        switch (DishManager.instance.counter)
        {
            case 1:
                zValue = sittingPos[0];
                break;
            case 2:
                zValue = sittingPos[1];
                break;
            case 3:
                zValue = sittingPos[2];
                break;
            default:
                break;
        }

    }

    void Update()
    {
        
        //Debug.Log("X is " + pixelUV.x +"  " + "Y is " + pixelUV.y);
        //tex.SetPixel((int)pixelUV.x, (int)pixelUV.y, Color.black);
        //tex.Apply();
        if (currentCount < maxCount && startSeqComp)
        {
            StartBrushing();
            
        }
        else if (currentCount >= maxCount && !letStop )
        {
            DishManager.instance.glitterParticle.Play();
            DishManager.instance.plateCounter++;
            dishChld = this.gameObject.transform.GetChild(0);
            MeshRenderer mesh = dishChld.GetComponent<MeshRenderer>();
            mesh.material.mainTexture = mappingTexture;
            DishManager.instance.counter++;
            
            letStop = true;
            StartCoroutine(EndSequence());
        }
    }

    private void StartBrushing()
    {
        if (!Input.GetMouseButton(0))
            return;

        RaycastHit hit;
        if (!Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hit))
            return;

        Renderer rend = hit.transform.GetComponent<Renderer>();
        MeshCollider meshCollider = hit.collider as MeshCollider;

        if (rend == null || rend.sharedMaterial == null || rend.sharedMaterial.mainTexture == null || meshCollider == null)
            return;

        Texture2D tex = rend.material.mainTexture as Texture2D;
        Vector2 pixelUV = hit.textureCoord;
        pixelUV.x *= tex.width;
        pixelUV.y *= tex.height;

        DrawBrush(pixelUV.x, pixelUV.y, 30);
    }


    private void DrawBrush(float centerX, float centerY, int radius)
    {
        int left = (int)centerX - radius;
        int right = left + radius * 2;
        int top = (int)centerY - radius;
        int bottom = top + radius * 2;

        for(int j = top; j <= bottom; ++j)
        {
            for(int k = left; k<= right; ++k)
            {
                if ((k + j * mappingTexture.width) <= DirtyTexColors.Length)
                {
                    index = k + j * mappingTexture.width;
                }
                
                
                if(progArray[index])
                {
                    continue;
                }
                double dist = Mathf.Pow((int)centerX - k, 2) + Mathf.Pow((int)centerY - j, 2);
                if(dist <= Mathf.Pow(radius, 2))
                {
                    
                    
                    DirtyTexColors[index] = CleanTexColors[index];
                    progArray[index] = true;
                    currentCount++;
                    
                    
                    DishManager.instance.totalCount++;
                    //DishManager.instance.progressBar.fillAmount = DishManager.instance.totalCount / 134000;

                    //counter++;
                }
                
            }
        }

        copyTexture.SetPixels(DirtyTexColors, 0);
        copyTexture.Apply();

    }

    IEnumerator StartSequence()
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitForSeconds(0f);
        Sequence initialSequence = DOTween.Sequence();
        initialSequence.Append(transform.DOMoveY(2.08f, .5f).SetEase(Ease.OutExpo)).OnComplete(() => startSeqComp = true);
    }

    IEnumerator EndSequence()
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitForSeconds(1f);
        Sequence startSequence = DOTween.Sequence();
        startSequence
                     .Append(transform.DOMoveZ(zValue, .5f).SetEase(Ease.OutExpo))
                     .Join(transform.DORotate(new Vector3(90f, 0f, 0f), .5f).SetEase(Ease.OutExpo))
                     .Append(transform.DOMoveY(1.725f, .5f))
                     .OnComplete(() => DishManager.instance.isInstantiated = false);

    }
}