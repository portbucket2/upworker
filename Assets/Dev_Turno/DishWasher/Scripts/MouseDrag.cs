﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MouseDrag : MonoBehaviour
{
    private float cameraZDistance;

    private Vector3 startPos;
    private Camera cam;
    private float lerpDuration = 3;
    float timeElapsed;

    private void Start()
    {
        cam = Camera.main;
        cameraZDistance = cam.WorldToScreenPoint(transform.position).z - 1.15f;
        startPos = transform.position;

        
    }
    private void Update()
    {
        if(Input.GetMouseButton(0))
        {
            MouseFollow();
        }
        else if(Input.GetMouseButtonUp(0))
        {
            SnapToTray();
        }
    }

    private void MouseFollow()
    {
        Vector3 screenPosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, cameraZDistance);
        Vector3 newWorldPosition = cam.ScreenToWorldPoint(screenPosition);
        transform.position = newWorldPosition;
    }
    private void SnapToTray()
    {

        //transform.DOMove(startPos, 1).SetEase(Ease.Flash);
        transform.position = Vector3.Lerp(startPos, transform.position, 0);

    }

}
