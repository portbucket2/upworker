﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScratchOn : MonoBehaviour
{
    public GameObject maskPrefab;
    public GameObject holder;
    private bool isPressed = false;
    private bool isRightLayer = false;


    [SerializeField]
    private Camera cam;
    
    // Update is called once per frame
    void Update()
    {
        var mousePos = Input.mousePosition;
        mousePos.z = 1.2f;
        
        mousePos = cam.ScreenToWorldPoint(mousePos);

        if(isPressed && isRightLayer)
        {
            GameObject maskSprite = Instantiate(maskPrefab, mousePos, Quaternion.Euler(90,0,0));
            maskSprite.transform.parent = gameObject.transform;
        }
        else
        {

        }
    
        if(Input.GetMouseButtonDown(0))
        {
            isPressed = true;
        }
        else if(Input.GetMouseButtonUp(0))
        {
            isPressed = false;
        }
    }

    private void FixedUpdate()
    {
        int layerMask = 1 << 8;
        //Vector3 mousePos = Input.mousePosition;
        //mousePos.z = 1.2f;
        //mousePos = cam.ScreenToWorldPoint(mousePos);

        RaycastHit hit;
        if (Physics.Raycast(holder.transform.position, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity, layerMask))
        {
            Debug.DrawRay(holder.transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            isRightLayer = true;
            Debug.Log("right");
        }
        else
        {
            isRightLayer = false;
        }
    }
}
