﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using FRIA;
public class DishManager : MonoBehaviour
{
    public static DishManager instance;
    [SerializeField]
    private List<GameObject> dishes = new List<GameObject>();
    public bool isInstantiated;
    public int counter;
    public Vector3[] dishPos;
    [SerializeField]
    private List<GameObject> fakePlates = new List<GameObject>();

    
    public int totalCount;
    public Image progressBar;
    public Image bar;
    public Text bannerText;
    public Text levelComp;

    public float hudai;
    public int plateCounter;
    
    public ParticleSystem glitterParticle;

    // Start is called before the first frame update
    void Start()
    {
        if(instance == null)
        {
            instance = this;
        }
        counter = 1;
        isInstantiated = false;
        totalCount = 0;
        progressBar.fillAmount = 0;
        ScaleText();
    }

    // Update is called once per frame
    void Update()
    {
        if(!isInstantiated)
        {
            DishMaker();
        }
        if(!isCompleted) Progression(); 
    }

    void DishMaker()
    {
        switch (counter)
        {
            case 1:
                GameObject d1 = Instantiate(dishes[0], dishPos[0], Quaternion.identity);
                Destroy(fakePlates[0]);
                break;
            case 2:
                GameObject d2 = Instantiate(dishes[1], dishPos[1], Quaternion.identity);
                Destroy(fakePlates[1]);
                break;
            case 3:
                GameObject d3 = Instantiate(dishes[1], dishPos[2], Quaternion.identity);
                Destroy(fakePlates[2]);
                break; 
            default:
                break;
        }
        isInstantiated = true;
    }

    bool isCompleted;
    void Progression()
    {
        hudai = totalCount * 0.00000762f;
        
        if(hudai >= 1)
        {
            progressBar.fillAmount = 1;
            if(plateCounter == 3)
            {
                //yield return new WaitForEndOfFrame();
                //yield return new WaitForSeconds(3.5f);
                isCompleted = true;
                Centralizer.Add_DelayedMonoAct(this, ()=>{
                    bar.gameObject.SetActive(false);
                    bannerText.gameObject.SetActive(false);

                    EndGameUI.Instance.LoadEndGameUI();
                },2.5f);

            }
            
        }
        else
        {
            progressBar.fillAmount = hudai;
            
        }
    }

    void ScaleText()
    {
        bannerText.transform.DOScale(new Vector2(1.2f, 1.2f), .7f).SetLoops(-1, LoopType.Yoyo);
    }
}
