﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.faithstudio.Gameplay;
using com.alphapotato.Gameplay;
using FRIA;


public enum GameState { START, PLUNG, SUCCESS, FAIL, END}
public class GameManager_Toilet : MonoBehaviour
{
    #region Public Variables
    public static GameManager_Toilet instance;
    public GameState state;

    public PowerBarController powerBar;
    public UIManager_Toilet uiManager;

    public ParticleSystem poopParticle;
    #endregion


    #region Private Variables
    [SerializeField]
    private Animator C_Controller;
    [SerializeField]
    private Animator P_Controller;

    private int IDLE = Animator.StringToHash("idle");
    private int CLEAN = Animator.StringToHash("clean");
    private int SUCCESS = Animator.StringToHash("success");
    private int FAIL = Animator.StringToHash("fail");
    private int PLUNG = Animator.StringToHash("plung");

    //private int successCount;
    private bool isFailDone;
    public bool isEnd;
    #endregion

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

    }


    private void Start()
    {
        //successCount = 0;
        isFailDone = false;
        isEnd = false;
        state = GameState.START;
        SetTrigger(C_Controller, CLEAN);
        SetTrigger(P_Controller, CLEAN);
        Init();

    }

    void SetTrigger(Animator anim, int trigger)
    {
        anim.SetTrigger(trigger);
    }

    void Init()
    {
        StartCoroutine(Initialize());
    }

    IEnumerator Initialize()
    {
        yield return new WaitForEndOfFrame();
        state = GameState.PLUNG;
        uiManager.ResetBool();
        
        Plunge();
    }

    void Plunge()
    {
        if (state != GameState.PLUNG)
            return;
        GlobalTouchController.Instance.EnableTouchController();
        StartCoroutine(StartPlunging());

    }

    IEnumerator StartPlunging()
    {
        //yield return new WaitForEndOfFrame();
        bool t_IsPlayerTappedScreen = false;
        powerBar.ShowPowerBar(delegate
        {
            t_IsPlayerTappedScreen = true;
            SetTrigger(C_Controller, PLUNG);
            SetTrigger(P_Controller, PLUNG);

        });
        powerBar.OnPassingTheAccuracyEvaluation += uiManager.UpdateCounter;

        WaitUntil t_WaitUntilPlayerTappedScreen = new WaitUntil(() =>
        {
            if (!t_IsPlayerTappedScreen)
                return false;
            return true;
        });
        yield return t_WaitUntilPlayerTappedScreen;
        powerBar.OnPassingTheAccuracyEvaluation -= uiManager.UpdateCounter;
        if(uiManager.isSuccess)
        {
            
            uiManager.currentVal += uiManager.incPerPush;
        }
        else if(uiManager.isFail)
        {
            //successCount = 0;
            Fail();
            //yield return new WaitForSeconds(1.5f);
            //SetTrigger(C_Controller, CLEAN);
            //SetTrigger(P_Controller, CLEAN);
            //yield return new WaitForSeconds(1f);
        }

        if ((uiManager.currentVal < uiManager.maxVal))
            Init();
        else if (uiManager.currentVal >= uiManager.maxVal)
            End();
        
       


    }

    void Fail()
    {
        state = GameState.FAIL;
        StartCoroutine(StartFailing());
    }

    IEnumerator StartFailing()
    {
        yield return new WaitForEndOfFrame();
        poopParticle.Play();
        //SetTrigger(C_Controller, FAIL);
        //SetTrigger(P_Controller, FAIL);
        
        isFailDone = true;
        //play the poop particle
    }


    void End()
    {
        state = GameState.END;
        StartCoroutine(StartEnding());
    }
    IEnumerator StartEnding()
    {
        yield return new WaitForEndOfFrame();
        SetTrigger(C_Controller, SUCCESS);
        SetTrigger(P_Controller, SUCCESS);

        EndGameUI.Instance.LoadEndGameUI();

        isEnd = true;
        InGameUI.SideProgBar.LoadValue(uiManager.maxVal,uiManager.maxVal);
    }
}
