﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager_Toilet : MonoBehaviour
{
   
    public int counter;
    public bool isSuccess;
    public bool isFail;

    public float maxVal = 100;
    public float incPerPush = 25;
    public float decPerSec = 10;
    public float currentVal = 0;



    private float addValue;



    

    void Start()
    {
        counter = 5;
        addValue = ((float)1/counter);
        isSuccess = false;
        isFail = false;
        InGameUI.SideProgBar.LoadValue(0,1, true);
    }

    private void Update()
    {
        if(!GameManager_Toilet.instance.isEnd)
        {
            UpdateProgBar();
        }
    }

    void UpdateProgBar()
    {
        currentVal -= decPerSec * Time.deltaTime;
        if (currentVal < 0)
        {
            currentVal = 0;
        }
        InGameUI.SideProgBar.LoadValue(currentVal, maxVal);
    }



    public void UpdateCounter(float value)
    {
       
        if(value >= 0.70)
        {
            //progressBar.fillAmount += addValue;
            isSuccess = true;
        }
        else
        {
            //progressBar.fillAmount = 0;
            isFail = true;
        }
        
        //counterText.text = counter.ToString();
    }

    public void ResetBool()
    {
        isSuccess = false;
        isFail = false;
    }
}
