﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using FRIA;
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(BuildingController))]
public class BuildingControllerEditor : BaseEditor, ICustomEditor
{
    string ICustomEditor.ScriptTitle { get { return "BuildingController"; } }
    string ICustomEditor.EditorTitle { get { return "BuildingControllerEditor"; } }
    //Type ICustomEditor.scriptType { get { return typeof(BuildingController); } }
    public override void OnCustomInspectorGUI()
    {
        base.OnCustomInspectorGUI();

        BuildingController bc = (BuildingController)target;
        Button("Prepare Mesh", () => {
            if (bc.meshTarget)
            {
                bc.outlineRenderer = bc.meshTarget.gameObject.GetComponent<Outline>();
                if (!bc.outlineRenderer)
                {
                    bc.outlineRenderer = bc.meshTarget.gameObject.AddComponent<Outline>();
                }
                bc.outlineRenderer.precomputeOutline = true;
                bc.outlineRenderer.ForceValidate();
                EditorFix.SetObjectDirty(bc);
                EditorFix.SetObjectDirty(bc.outlineRenderer);
            }
        
        });
    }
}
#endif