﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildingController : MonoBehaviour
{

    public BuildingType type;
    public Transform personalUI;
    public MeshRenderer meshTarget;
    public Outline outlineRenderer;
    public Text titleText;
    UnityEngine.UI.Outline textOutline;

    public BoxCollider inputCollider;

    bool buildingIsActive;
    bool buildingIsSelected;

    internal List<TaskProfile> tasksOffTopic = new List<TaskProfile>();
    internal List<TaskProfile> tasksOnTopic = new List<TaskProfile>();

    public BuildingStatusSettings selectedStatus;
    public BuildingStatusSettings unselectedStatus;
    public BuildingStatusSettings inactiveStatus;


    public static List<BuildingController> activeBuildings = new List<BuildingController>();
    private void Awake()
    {
        CityViewManager.onNewBuildingSelected += OnNewBuildingSelected;
        textOutline = titleText.GetComponent<UnityEngine.UI.Outline>();
    }

    private void OnEnable()
    {
        activeBuildings.Add(this);
    }
    private void OnDisable()
    {
        activeBuildings.Remove(this);
    }

    private void OnDestroy()
    {
        CityViewManager.onNewBuildingSelected -= OnNewBuildingSelected;
    }

    public void Load(int accessibleCount)
    {
        buildingIsActive = tasksOnTopic.Count > 0;

        //personalUI.gameObject.SetActive(buildingIsActive);
        inputCollider.enabled = buildingIsActive;
        if (buildingIsActive)
            titleText.text = string.Format("{0} ({1}/{2})", type, accessibleCount, tasksOnTopic.Count);
        else
            titleText.text = string.Format("{0}", type);

        personalUI.transform.rotation = Camera.main.transform.rotation;
        SetUnselected();
    }

    public void OnNewBuildingSelected(BuildingType type)
    {
        if (!buildingIsActive) return;

        if (type == this.type)
        {
            SetSelected();
        }
        else
        {
            SetUnselected();
        }
    }

    void SetSelected()
    {

        buildingIsSelected = true;
        ApplyStatus();
        CityViewManager.Instance.LoadBuildingPanel(this);
    }
    void SetUnselected()
    {
        buildingIsSelected = false;
        ApplyStatus();
    }

    void ApplyStatus()
    {
        BuildingStatusSettings bss;
        if (!buildingIsActive)
        {
            bss = inactiveStatus;
        }
        else if (buildingIsSelected)
        {
            bss = selectedStatus;
        }
        else
        {
            bss = unselectedStatus;
        }

        outlineRenderer.enabled = bss.hasOutline;
        if (bss.hasOutline)
        {
            outlineRenderer.OutlineColor = bss.outlineColor;
            outlineRenderer.OutlineWidth = bss.outlineWidth;
        }
        textOutline.effectColor = bss.textOutlineColor;
        titleText.gameObject.SetActive(bss.hasText);
        if (bss.hasText)
        {
            titleText.fontSize = bss.fontSize;
        }
    }
}

[System.Serializable]
public class BuildingStatusSettings
{
    public bool hasOutline = true;
    public bool hasText = true;
    public Color outlineColor = new Color(0,1,0.2f,1);
    public Color textOutlineColor = new Color(0, 1, 0.2f, 1);
    public float outlineWidth = 6.6f;
    public int fontSize = 35;
}