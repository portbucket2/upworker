﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HomeUI : MonoBehaviour
{
    public Button jobsButton;
    public Button workNowButton;

    private void Start()
    {
        jobsButton.onClick.AddListener(OnJobs);
        workNowButton.onClick.AddListener(OnWork);
    }
    void OnJobs()
    {
        if (DayCycle.OutOfStamina) return;
        SceneData.LoadCity();
    }
    void OnWork()
    {
        if (DayCycle.OutOfStamina) return;
        SceneData.LoadGameplay(JobRoster.activeJob);
    }

}
