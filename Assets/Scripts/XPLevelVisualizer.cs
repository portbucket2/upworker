﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class XPLevelVisualizer : MonoBehaviour
{
    
    public Text lvlDisplayText;
    public BarUI xpBar;
    public bool animateBarOnLoad = false;

    int savedValueLVL;

    void Start()
    {

        XPLevelUpData xpdt = PlayerProgressManager.Instance.AddXP(0) ;
        xpBar.LoadValue(xpdt.xp_f,xpdt.xp_f_max, instant: !animateBarOnLoad);
        savedValueLVL = xpdt.lvl_f;
        SetText(savedValueLVL);
        PlayerProgressManager.onXpUpdate+=OnXPChanged;
    }
    void OnXPChanged(XPLevelUpData data)
    {
        SetText(data.lvl_i);
        xpBar.LoadValue(data.xp_i, data.xp_i_max, true);
        if (data.lvl_f == data.lvl_i)
        {

            xpBar.LoadValue(data.xp_f, data.xp_f_max);
        }
        else
        {
            xpBar.LoadValue(data.xp_i_max, data.xp_i_max,onExpectedToBeDone: ()=>
            {
                SetText(data.lvl_f);
                xpBar.LoadValue(0, data.xp_f_max, true);
                xpBar.LoadValue(data.xp_f, data.xp_f_max);
            });
        }

    }
    void SetText(int value) => lvlDisplayText.text = string.Format("Level {0}",value);

    private void OnDestroy()
    {
        PlayerProgressManager.onXpUpdate -= OnXPChanged;
    }
}
