﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FRIA;

public class InterviewLoader : MonoBehaviour
{

    public Animator bossanim;
    public TaskProfile profile;
    public Text questionText;

    public GameObject questionRoot;
    public GameObject answersRoot;

    public GameObject acceptedItem;
    public GameObject rejectedItem;

    public List<InterviewAnswer> answers = new List<InterviewAnswer>();
    public List<GameObject> interviewerMeshes;
    void Start()
    {
        questionRoot.SetActive(true);
        answersRoot.SetActive(true);
        acceptedItem.SetActive(false);
        rejectedItem.SetActive(false);
        profile = SceneData.runningGameProfile;
        int interviewerChoice = 0;
        switch (profile.buildingType)
        {
            case BuildingType.Cafe:
                interviewerChoice = 1;
                break;
            case BuildingType.Factory:
                interviewerChoice = 0;
                break;
            case BuildingType.Mall:
                interviewerChoice = 2;
                break;

        }
        for (int i = 0; i < interviewerMeshes.Count; i++)
        {
            interviewerMeshes[i].SetActive(i==interviewerChoice);
        }

        questionText.text =profile.interviewQuestion;
        int rightChoice = Random.Range(0, answers.Count);

        for (int i = 0; i < answers.Count; i++)
        {
            if (i == rightChoice)
            {
                answers[i].text.text = profile.answerRight;
                answers[i].button.onClick.RemoveAllListeners();
                answers[i].button.onClick.AddListener(OnRight);
            }
            else
            {
                answers[i].text.text = profile.answerWrong;
                answers[i].button.onClick.RemoveAllListeners();
                answers[i].button.onClick.AddListener(OnWrong);
            }
        }
    }

    void OnRight()
    {
        bossanim.SetTrigger("success");
        questionRoot.SetActive(false);
        answersRoot.SetActive(false);
        acceptedItem.SetActive(true);
        Centralizer.Add_DelayedMonoAct(this,()=> { SceneData.LoadGameplay(profile); },2);
    }
    void OnWrong()
    {
        bossanim.SetTrigger("fail");
        questionRoot.SetActive(false);
        answersRoot.SetActive(false);
        rejectedItem.SetActive(true);
        Centralizer.Add_DelayedMonoAct(this, () => { SceneData.LoadCity(CityInitType.LATEST_JOBS); }, 2);
    }

}

[System.Serializable]
public class InterviewAnswer
{
    public Text text;
    public Button button;
}