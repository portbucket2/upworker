﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FRIA;
public class CityViewManager : MonoBehaviour
{
    public static CityViewManager Instance { get; private set; }

    public static event System.Action<BuildingType> onNewBuildingSelected;
    [Header("References")]
    public MapPanController panner;
    public DragReporter dragReporter;
    public Camera cam;
    public Button homeButton;

    [Header("Tabs")]
    public List<BasicTabController> tabButtons;
    public Color currentTabColor = new Color(1, .5f, .5f, 1);
    public Color otherTabColor = new Color(1, .75f, .75f, 1);
    public TaskCategory currentCategory;//better not initialize

    [Header("Buildings")]
    public Dictionary<BuildingType, BuildingController> buildingTable = new Dictionary<BuildingType, BuildingController>();



    [Header("Joblist UI")]
    public Transform jobListRoot;
    public Text buildingTitleText;
    public GameObject jobuiPref;


    internal TaskProfile latestJob;
    List<JobItemUI> itemList = new List<JobItemUI>();

    public static CityInitType INIT_TYPE = CityInitType.JOB;
    private void Awake()
    {
        PersistentItem.ActivateChildren("CityMap", true);
    }
    private void OnDestroy()
    {
        PersistentItem.ActivateChildren("CityMap", false);
    }

    private void Start()
    {
        Instance = this;
        //foreach (var item in BuildingController.activeBuildings)
        //{
        //    buildingTable.Add(item.type,item);
        //}

        for (int i = 0; i < tabButtons.Count; i++)
        {
            int index = i;
            tabButtons[index].Init(index==0, () => { OnTabButtonClick(index); });
        }
        SetTabState((int)currentCategory);

        dragReporter.onTap += onTap;
        CityInit(INIT_TYPE);

        homeButton.onClick.AddListener(() => { SceneData.LoadHome(); });
    }


    void SetTabState(int selectedIndex )
    {
        TaskCategory oldCat = currentCategory;
        currentCategory = (TaskCategory)selectedIndex;
        for (int i = 0; i < tabButtons.Count; i++)
        {
            tabButtons[i].SetState(i == selectedIndex);
        }
    }
    void OnTabButtonClick(int index)
    {
        SetTabState(index);
        switch (currentCategory)
        {
            case TaskCategory.job:
                CityInit(CityInitType.JOB);
                break;
            case TaskCategory.training:
                CityInit(CityInitType.TRAINING);
                break;
            default:
                Debug.LogError("Undefined");
                break;
        }

    }


    BuildingController trainingBuildingInitial;
    void CityInit(CityInitType initType)
    {
        JobRoster.Instance.UpdateVisibleTaskList();
        latestJob = JobRoster.activeJob;
        List<TaskProfile> tasks = JobRoster.visibleTasks;

        trainingBuildingInitial = null;
        foreach (BuildingController building in BuildingController.activeBuildings)
        {
            building.tasksOnTopic.Clear();
            building.tasksOffTopic.Clear();
            int levelAccessible = 0;
            foreach (var task in tasks)
            {
                if (task.buildingType == building.type)
                {
                    if (task.taskCategory == currentCategory)
                    {
                        if (task.taskCategory == TaskCategory.training && !trainingBuildingInitial)
                        {
                            trainingBuildingInitial = building;
                        }
                        levelAccessible += (task.IsTaskLevelAccessible() ? 1 : 0);
                        building.tasksOnTopic.Add(task);
                    }
                    else
                    {
                        building.tasksOffTopic.Add(task);
                    }
                    //Debug.LogFormat("building {0}- task {1}: ontopic = {2}",building.type, task.taskTitle, task.taskCategory);
                }
            }
            building.Load(levelAccessible);
        }

        //Debug.LogFormat("B_{0}", INIT_TYPE);
        switch (initType)
        {
            case CityInitType.JOB:
                {
                    if (latestJob != null)
                        InvokeBuildingSelection(latestJob.buildingType);
                    else
                        InvokeBuildingSelection(tasks[0].buildingType);
                }
                break;
            case CityInitType.TRAINING:
                {
                    InvokeBuildingSelection(trainingBuildingInitial.type);
                }
                break;
            case CityInitType.LATEST_JOBS:
                {
                    //Debug.LogFormat("C_{0}", INIT_TYPE);
                    TaskProfile highLevelJob = null;
                    for (int i = 0; i < tasks.Count; i++)
                    {
                        if (tasks[i].taskCategory== TaskCategory.job && tasks[i].requiredLevel==Currency.Balance(CurrencyType.LVL))
                        {
                            highLevelJob = tasks[i];
                            break;
                        }
                    }
                    if (highLevelJob != null)
                    {
                        //Debug.LogFormat("D_{0}", highLevelJob.buildingType);
                        InvokeBuildingSelection(highLevelJob.buildingType);
                    }
                    else
                    {
                        InvokeBuildingSelection(latestJob.buildingType);
                    }

                }
                break;
           // case CityInitType.SPECIFIC:
               // break;
            default:
                Debug.LogError("Undefined");
                break;
        }

        //switch (currentCategory)
        //{
        //    case TaskCategory.job:
        //        if (latestJob != null)
        //            InvokeBuildingSelection(latestJob.buildingType);
        //        else
        //            InvokeBuildingSelection(tasks[0].buildingType);
        //        break;
        //    case TaskCategory.training:
        //        {
        //            InvokeBuildingSelection(trainingBuildingInitial.type);
        //        }
        //        break;
        //}

    }



    private void onTap(Vector3 mousePos)
    {
        Ray camray = cam.ScreenPointToRay(mousePos);
        RaycastHit rch;
        if (Physics.Raycast(camray, out rch, 100))
        {
            BuildingController bc = rch.collider.gameObject.GetComponentInParent<BuildingController>();
            //Debug.Log(bc);
            if (bc)
            {
                InvokeBuildingSelection(bc.type);
            }
        }
    }

    void InvokeBuildingSelection(BuildingType type)
    {
        //Debug.Log("E");
        BuildingController bc=null;
        foreach (var item in BuildingController.activeBuildings)
        {
            if (item.type == type) bc = item;
        }
        if(bc)
            //Centralizer.Add_DelayedMonoAct(this,()=> { 
            panner.Focus(bc.transform); 
        //},0.2f) ;
        onNewBuildingSelected?.Invoke(type);
    }

    public void LoadBuildingPanel(BuildingController bc)
    {
        buildingTitleText.text = string.Format("{0}",bc.type);

        int i;
        for (i = 0; i < bc.tasksOnTopic.Count; i++)
        {
            JobItemUI jui;
            if (i < itemList.Count)
            {
                jui = itemList[i];
            }
            else
            {
                GameObject go = Instantiate(jobuiPref,jobListRoot);
                go.transform.rotation = Quaternion.identity;
                go.transform.localScale = Vector3.one;
                jui = go.GetComponent<JobItemUI>();
                itemList.Add(jui);
            }
            jui.Load(bc.tasksOnTopic[i]);
        }
        for (; i < itemList.Count; i++)
        {
            itemList[i].gameObject.SetActive(false);
        }
    }


}
public enum CityInitType
{
    JOB,//current job or first job in list
    TRAINING,//first training
    LATEST_JOBS, // latest level job
              //SPECIFIC, //building with specific task profile
}