﻿using UnityEngine;
using UnityEngine.UI;


public class JobItemUI : MonoBehaviour
{
    public Text jobTitle;
    public Text requirementText;
    public Text cashEarningText;
    public Text xpEarningText;

    [Header("Current Job")]
    public GameObject currentWorkObject;
    public Button workNowButton;
    [Header("New Job")]
    public GameObject applyNewObject;
    public Text applyCostText;
    public Button applyNowButton;
    public Text applyCostTitleText;
    [Header("Training")]
    public GameObject trainingObject;
    public Text trainingCostText;
    public Text trainingCostTitleText;
    public Button trainNowButton;


    TaskProfile profile;

    bool colorsLoaded = false;
    public Color notEnoughLevelColor;
    public Color notEnoughCashColor;

    Color lvlColorBase;
    Color applyCashColorBase;
    Color trainCashColorBase;

    public void Load(TaskProfile profile)
    {
        if (!colorsLoaded)
        {
            lvlColorBase = requirementText.color;
            applyCashColorBase = applyCostText.color;
            trainCashColorBase = trainingCostText.color;
            colorsLoaded = true;
        }

        this.profile = profile;
        gameObject.SetActive(true);

        jobTitle.text = profile.title;
        cashEarningText.text = string.Format("income: {0}$",profile.cashReward);
        xpEarningText.text = string.Format("xp: {0}", profile.xpReward);

        bool doesPlayerHaveEnoughCash = Currency.Balance(CurrencyType.CASH) >= profile.entryCost;

        bool isLevelHighEnough = PlayerProgressManager.currentLevel >=profile.requiredLevel;

        switch (profile.taskCategory)
        {
            case TaskCategory.job:
                {
                    bool isNewJob = JobRoster.activeJobID.value != profile.taskID;
                    applyNewObject.SetActive(isNewJob);
                    currentWorkObject.SetActive(!isNewJob);
                    trainingObject.SetActive(false);

                    cashEarningText.gameObject.SetActive(true);
                    if (isNewJob)
                    {
                        applyCostText.text = string.Format("{0}$", profile.entryCost);
                        applyNowButton.interactable = isLevelHighEnough && doesPlayerHaveEnoughCash;
                        applyCostText.color = doesPlayerHaveEnoughCash ? applyCashColorBase : notEnoughCashColor;
                    }
                    else
                    {

                        workNowButton.interactable = true;
                    }
                }
                break;
            case TaskCategory.training:
                {
                    applyNewObject.SetActive(false);
                    currentWorkObject.SetActive(false);
                    trainingObject.SetActive(true);

                    cashEarningText.gameObject.SetActive(false);

                    trainingCostText.text = string.Format("{0}$", profile.entryCost);
                    trainNowButton.interactable = isLevelHighEnough && doesPlayerHaveEnoughCash;
                    trainingCostText.color = doesPlayerHaveEnoughCash ? trainCashColorBase : notEnoughCashColor;
                }
                break;
            default:
                Debug.LogError("Undefined category");
                break;
        }


        if (PlayerProgressManager.currentLevel >= profile.requiredLevel)
        {
            requirementText.text = string.Format("Job level {0}", profile.requiredLevel);
            requirementText.color = lvlColorBase;
        }
        else
        {
            requirementText.text = string.Format("Required level {0}",profile.requiredLevel);
            requirementText.color = notEnoughLevelColor;
        }

    }
    private void Awake()
    {
        applyNowButton.onClick.AddListener(OnApplyNow);
        workNowButton.onClick.AddListener(OnWorkNow);
        trainNowButton.onClick.AddListener(OnTrainNow);
    }

    const float LOAD_DELAY=0.75f;
    void OnApplyNow()
    {
        applyNowButton.interactable = false;
        Currency.Transaction(CurrencyType.CASH, -profile.entryCost);
        FRIA.Centralizer.Add_DelayedMonoAct(this, () => { SceneData.LoadInterview(profile); }, LOAD_DELAY);
    }
    void OnTrainNow()
    {

        Currency.Transaction(CurrencyType.CASH, -profile.entryCost);
        if (PlayerProgressManager.Instance.enableInstaCompletion)
        {
            PlayerProgressManager.GetDirectTaskReward(profile);
            trainNowButton.interactable = Currency.Balance(CurrencyType.CASH) >= profile.entryCost;
            return;
        }
        else
        {

            trainNowButton.interactable = false;
            FRIA.Centralizer.Add_DelayedMonoAct(this, () => { SceneData.LoadGameplay(profile); }, LOAD_DELAY);
        }

    }
    void OnWorkNow()
    {
        if (PlayerProgressManager.Instance.enableInstaCompletion)
        {
            PlayerProgressManager.GetDirectTaskReward(profile);
            return;
        }
        else
        {

            workNowButton.interactable = false;
            FRIA.Centralizer.Add_DelayedMonoAct(this, () => { SceneData.LoadGameplay(profile); }, LOAD_DELAY);
        }
    }
}
