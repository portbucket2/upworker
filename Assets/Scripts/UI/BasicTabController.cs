﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BasicTabController : MonoBehaviour
{
    public Button tabButton;

    public Image tabimage;
    public Sprite selectedImage;
    public Sprite unselectedImage;


    public void Init(bool selected, System.Action onclick)
    {
        SetState(selected);
        tabButton.onClick.AddListener(()=> { onclick?.Invoke(); });
    }

    public void SetState(bool selected)
    {
        tabimage.sprite = selected ? selectedImage : unselectedImage;
        tabButton.interactable = !selected;
    }

}

