﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using FRIA;

public class DayCycle : MonoBehaviour 
{
    public static event System.Action onEndOfDay;


    public const int MaxStamina = 5;
    public const int RoomCost = -10;
    public const int FoodCost = -5;

    #region hard data
    private static HardData<int> consumedStaminaHD;
    private static int consumedStamina
    {
        get
        {
            if (consumedStaminaHD == null) consumedStaminaHD = new HardData<int>("CONSUMED_STAMINA", 0);
            return consumedStaminaHD.value;
        }
        set
        {
            if (consumedStaminaHD == null) consumedStaminaHD = new HardData<int>("CONSUMED_STAMINA", 0);
            consumedStaminaHD.value = value;

        }

    }


    private static HardData<int> todaysIncome_HD;
    public static int todaysIncome
    {
        get
        {
            if (todaysIncome_HD == null) todaysIncome_HD = new HardData<int>("TODAY_INCOME", 0);
            return todaysIncome_HD.value;
        }
        set
        {
            if (todaysIncome_HD == null) todaysIncome_HD = new HardData<int>("TODAY_INCOME", 0);
            todaysIncome_HD.value = value;

        }

    }


    private static HardData<int> todaysXP_HD;
    public static int todaysXP
    {
        get
        {
            if (todaysXP_HD == null) todaysXP_HD = new HardData<int>("TODAY_XP", 0);
            return todaysXP_HD.value;
        }
        set
        {
            if (todaysXP_HD == null) todaysXP_HD = new HardData<int>("TODAY_XP", 0);
            todaysXP_HD.value = value;

        }

    }


    private static HardData<int> lastDayCash_HD;
    private static int lastDayCash
    {
        get
        {
            if (lastDayCash_HD == null) lastDayCash_HD = new HardData<int>("LASTDAY_CASH", 0);
            return lastDayCash_HD.value;
        }
        set
        {
            if (lastDayCash_HD == null) lastDayCash_HD = new HardData<int>("LASTDAY_CASH", 0);
            lastDayCash_HD.value = value;

        }

    }
    #endregion

    #region stamina
    public static int CurrentStamina { get { return MaxStamina - consumedStamina; } }
    public static bool OutOfStamina { get { return CurrentStamina == 0; } }
    public static string staminaDispString { get { return string.Format("Stamina: {0}/{1}",CurrentStamina,MaxStamina); } }

    public static void ConsumeStamina()
    {
        consumedStamina++;
    }
    #endregion


    public Button endDayButton;
    public Button bgclickButton;

    public Text staminaDispText;
    //public Image staminaBar;


    public CurrencyTracker cashTracker;

    [Header("Day End Panel")]
    public GameObject dayEndPanel;
    public Text incomeText;
    public Text xpText;
    public Text cashDiffText;

    public Text roomCostText;
    public Text foodCostText;

    public Button doubleUpButton;
    public Button recoverButton;

    public Animator nightAnim;
    public float nightProgress;


    private IEnumerator Start()
    {
        staminaDispText.text = staminaDispString;
        endDayButton.onClick.AddListener(OnPanelClose);
        bgclickButton.onClick.AddListener(OnPanelClose);

        dayEndPanel.SetActive(false);
        yield return new WaitForSeconds(0.5f);
        if (CurrentStamina == 0) ShowDayEndUI();

    }

    int diffCash;
    public void ShowDayEndUI()
    {
        dayEndPanel.SetActive(true);
        incomeText.text = string.Format("Income: {0}$",todaysIncome);
        xpText.text = string.Format("{0}xp", todaysXP);

        roomCostText.text = string.Format("House Rent: {0}$", RoomCost);
        foodCostText.text = string.Format("Food Service: {0}$", FoodCost);

        diffCash = Currency.Balance(CurrencyType.CASH) - lastDayCash + RoomCost + FoodCost;
        cashDiffText.text = string.Format("{0}$",diffCash);
        //if (diffCash > 0)
        //{
        //    doubleUpButton.gameObject.SetActive(true);
        //    recoverButton.gameObject.SetActive(false);
        //}
        //else
        //{
        //    doubleUpButton.gameObject.SetActive(false);
        //    recoverButton.gameObject.SetActive(true);
        //}

    }
    void OnPanelClose()
    {
        nightAnim.SetTrigger("end");
        cashTracker.pauseExternal = true;
        Currency.Transaction(CurrencyType.CASH, FoodCost + RoomCost);
        consumedStamina = 0;
        lastDayCash = Currency.Balance(CurrencyType.CASH);
        todaysIncome = 0;
        todaysXP = 0;
        //staminaDispText.text = staminaDispString;
        dayEndPanel.SetActive(false);

        nightProgress = 0;
        StopAllCoroutines();
        StartCoroutine(NightSync());
    }
    IEnumerator NightSync()
    {
        while (nightProgress >= 0)
        {
            if (nightProgress < 1)
            {
                yield return null;
                continue;
            }
            else if (nightProgress <= 2)
            {
                cashTracker.pauseExternal = false;
                int shownStamina =Mathf.RoundToInt(  Mathf.Lerp(0,MaxStamina,nightProgress-1));
                staminaDispText.text = string.Format("Stamina: {0}/{1}", shownStamina, MaxStamina);

            }
            else
            {
                staminaDispText.text = staminaDispString;

            }
            yield return null;
        }
    }
    
}
