﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DummyGameplayScript : MonoBehaviour
{

    public Text title;
    public BarUI tapToFillbar;

    public bool completionReported = false;

    public float progress = 0;
    public float maxProgress = 100;

    private void Start()
    {
        if (SceneData.runningGameProfile == null)
        {
            SceneData.LoadCity();
            return;
        }

        progress = 0;
        tapToFillbar.LoadValue(progress, maxProgress,true);
        title.text = SceneData.runningGameProfile.title;
    }
    void Update()
    {
        if (!completionReported)
        {
            if (Input.GetMouseButtonUp(0))
            {
                progress = Mathf.Clamp(progress + 10, 0, maxProgress);
                tapToFillbar.LoadValue(progress, maxProgress);
            }
            else
            {
                progress = Mathf.Clamp(progress- 20*Time.deltaTime,0,maxProgress);
                tapToFillbar.LoadValue(progress, maxProgress);
            }

            if (progress >= maxProgress)
            {
                completionReported = true;
                EndGameUI.Instance.LoadEndGameUI();
            }
        }
    }
}
