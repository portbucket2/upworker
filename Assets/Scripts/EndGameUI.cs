﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FRIA;

public class EndGameUI : MonoBehaviour
{
    public static EndGameUI Instance { get; private set; }
    public static TaskProfile gameProfile 
    {
        get
        {
            return SceneData.runningGameProfile;
        }
    }


    [Header("Panels")]
    public List<GameObject> inGameOnlyObjs;

    public GameObject endGameUIRoot;
    public GameObject endGameMainPanel;
    public GameObject endGameLevelUpPanel;

    [Header("Main End Panel Items")]
    public GameObject jobCompleteSubPanel;
    public GameObject trainingCompleteSubPanel;
    public Text taskTitleText;

    [Header("JobComplete")] 
    public BarUI jobXPBar;
    public Text jobCashRewardText;
    public Text jobXPRewardText;
    public Button jobsButtonOnJobComplete;
    public Button workAgian;
    [Header("TrainingComplete")]
    public BarUI trainingXPBar;
    public Text trainingCostOnTrainAgainText;
    public Text trainingXPRewardText;
    public Button jobsButtonOnTrainingComplete;
    public Button trainAgain;


    [Header("Level Up Panel Items")]
    public Text reachedLevelText;

    [Header("Buttons")]
    public Button jobsButton_lvlUpPanel;


    private void Awake()
    {
        Instance = this;
        foreach (GameObject go in inGameOnlyObjs) go.SetActive(true);
        endGameUIRoot.SetActive(false);
        endGameMainPanel.SetActive(false);
        endGameLevelUpPanel.SetActive(false);

        workAgian.onClick.AddListener(OnPlayAgain);
        trainAgain.onClick.AddListener(OnTrainAgain);

        jobsButtonOnJobComplete.onClick.AddListener(OnJobs);
        jobsButtonOnTrainingComplete.onClick.AddListener(OnJobs);
        jobsButton_lvlUpPanel.onClick.AddListener(OnJobs);
    }
    void OnJobs()
    {
        if (DayCycle.OutOfStamina)
        {
            SceneData.LoadHome();
        }
        else
        {
            SceneData.LoadCity(CityInitType.LATEST_JOBS);
        }
    }

    void OnPlayAgain()
    {
        SceneData.LoadGameplay(gameProfile);
    }
    void OnTrainAgain()
    {
        if (gameProfile.entryCost <= Currency.Balance(CurrencyType.CASH))
        {
            Currency.Transaction(CurrencyType.CASH,-gameProfile.entryCost);
            SceneData.LoadGameplay(gameProfile);
        }

    }

    XPLevelUpData xpdata;
    public void LoadEndGameUI()
    {
        AnalyticsAssistant.MinigameCompleted(gameProfile);
        DayCycle.ConsumeStamina();
        foreach (GameObject go in inGameOnlyObjs) go.SetActive(false);
        endGameUIRoot.SetActive(true);
        endGameMainPanel.SetActive(true);
        endGameLevelUpPanel.SetActive(false);




        if (gameProfile!=null)
        {

            xpdata = PlayerProgressManager.Instance.AddXP(gameProfile.xpReward);
            Currency.Transaction(CurrencyType.CASH, gameProfile.cashReward);
            DayCycle.todaysIncome += gameProfile.cashReward;
            DayCycle.todaysXP += gameProfile.xpReward;


            taskTitleText.text = string.Format("{0}", gameProfile.title);

            bool isJob = gameProfile.taskCategory == TaskCategory.job;

            jobCompleteSubPanel.SetActive(isJob);
            trainingCompleteSubPanel.SetActive(!isJob);
            BarUI xpBar;
            if (isJob)
            {
                xpBar = jobXPBar;

                jobCashRewardText.text = string.Format("{0}$", gameProfile.cashReward);
                jobXPRewardText.text = string.Format("XP Earned: {0}", gameProfile.xpReward);

                workAgian.interactable = !DayCycle.OutOfStamina;
            }
            else
            {
                xpBar = trainingXPBar;

                trainingXPRewardText.text = string.Format("XP Earned: {0}", gameProfile.xpReward);
                trainingCostOnTrainAgainText.text = string.Format("{0}$", gameProfile.entryCost);

                trainAgain.interactable = (gameProfile.entryCost <= Currency.Balance(CurrencyType.CASH)) && !DayCycle.OutOfStamina;

            }
           
            xpBar.LoadValue(xpdata.xp_i, xpdata.xp_i_max, true);
            if (xpdata.lvl_f == xpdata.lvl_i)
            {
                xpBar.LoadValue(xpdata.xp_f, xpdata.xp_f_max);
            }
            else
            {
                AnalyticsAssistant.LevelCompleted(xpdata.lvl_i,gameProfile);
                AnalyticsAssistant.LevelStarted(xpdata.lvl_f);
                xpBar.LoadValue(xpdata.xp_i_max, xpdata.xp_i_max, onExpectedToBeDone: LoadLevelUpPanel);
            }
        }


    }

    void LoadLevelUpPanel()
    {

        reachedLevelText.text = string.Format("Reached Level {0}", xpdata.lvl_f);
        endGameMainPanel.SetActive(false);
        endGameLevelUpPanel.SetActive(true);
    }
}
