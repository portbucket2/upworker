﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameUI : MonoBehaviour
{
    public static InGameUI Instance { get; private set; }
    public Text gameTitle;
    [SerializeField] BarUI gameProgressBar;
    [SerializeField] BarUI powerMeterBar;
    public Text instruction;


    public static BarUI MainProgBar
    {
        get
        {
            return Instance.gameProgressBar;
        }
    }
    public static BarUI SideProgBar
    {
        get
        {
            return Instance.powerMeterBar;
        }
    }
    public static GameObject instructionObject
    {
        get
        {
            return Instance.instruction.gameObject;
        }
    }
    void Awake()
    {
        Instance = this;
        if (SceneData.runningGameProfile != null)
        {
            gameTitle.text = SceneData.runningGameProfile.title;
            SceneData.runningGameProfile.IncreasePlayCount();
        }
    }
}
