﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneData : MonoBehaviour
{
    static bool dummy_mode  = false;
    static string city_scene = "city_scene";
    static string home_scene = "home_scene";
    static string interview_scene = "interview_scene";


    public static TaskProfile runningGameProfile;
    public static void LoadGameplay(TaskProfile profile)
    {
        if (profile == null)
        {
            LoadCity();
        }
        else
        {
            if (profile.taskCategory == TaskCategory.job) JobRoster.activeJob = profile;
            runningGameProfile = profile;
            if (dummy_mode)
            {
                LoadDummy();
            }
            else
            {
                string sceneName = string.Format("Levels/{0}", profile.taskID);
                if (Application.CanStreamedLevelBeLoaded(sceneName))
                {
                    SceneManager.LoadScene(sceneName);
                }
                else
                {
                    LoadDummy();
                }

            }
        }

    }
    static  void LoadDummy()
    {
        SceneManager.LoadScene(string.Format("Gameplay_dummy"));
    }

    public static void LoadCity()
    {
        LoadCity(CityInitType.JOB);
    }
    public static void LoadCity(CityInitType initType)
    {
        CityViewManager.INIT_TYPE = initType;
        SceneManager.LoadScene(city_scene);
    }

    public static void LoadHome()
    {
        SceneManager.LoadScene(home_scene);
    }
    public static void LoadInterview(TaskProfile profile)
    {
        runningGameProfile = profile;
        SceneManager.LoadScene(interview_scene);
    }
}
