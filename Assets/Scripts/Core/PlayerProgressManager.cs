﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class PlayerProgressManager : MonoBehaviour
{
    public static PlayerProgressManager Instance { get; private set; }
    public static System.Action<XPLevelUpData> onXpUpdate;

    //public static HardData<int> currentXP;
    //public static HardData<int> currentLevel;
    public bool enableInstaCompletion = false;
    public static int currentXP 
    { 
        get 
        {
            return Currency.Balance(CurrencyType.XP);
        }
    }
    public static int currentLevel
    {
        get
        {
            return Currency.Balance(CurrencyType.LVL);
        }
    }
    public List<PlayerProgData> progressionDataList;

    public PlayerProgData GetCurrentProgData()
    {
        foreach (var progData in progressionDataList)
        {
            if (progData.level == currentLevel)
            {
                return progData;
            }
        }
        return null;
    }
    public PlayerProgData GetPrevProgData()
    {
        for (int i = 0; i < progressionDataList.Count; i++)
        {
            if (progressionDataList[i].level == currentLevel)
            {
                return progressionDataList[i-1];
            }
        }
        return null;
    }

    static HardData<bool> hasReportedLevel1Started;
    void Awake()
    {
        if (hasReportedLevel1Started == null) hasReportedLevel1Started = new HardData<bool>("has_reported_level_1_started", false);
        if(!hasReportedLevel1Started.value)
        {
            AnalyticsAssistant.LevelStarted(1);
            hasReportedLevel1Started.value = true;
        }

        if (Instance)
        {
#if UNITY_EDITOR
            Application.targetFrameRate = 30;
#endif
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
        }
    }


    public XPLevelUpData AddXP(int xp, XPLevelUpData data = null)
    {
        CurrencyType c_xp = CurrencyType.XP;
        PlayerProgData cpd = GetCurrentProgData();
        int xpMax = cpd.xpMax;

        if (data == null)
        {

            if (xpMax == -1)
            {
                data = new XPLevelUpData(Currency.Balance(CurrencyType.LVL), 0, 1);
            }
            else
            {
                data = new XPLevelUpData(Currency.Balance(CurrencyType.LVL), Currency.Balance(c_xp), cpd.xpMax);
            }
        }

        if (xpMax == -1)
        {
            Currency.Transaction(c_xp, -Currency.Balance(c_xp));
            data.lvl_f = Currency.Balance(CurrencyType.LVL);
            data.xp_f = 0;
            data.xp_f_max = 1;
            onXpUpdate?.Invoke(data);
            return data;
        } 
        else if (currentXP + xp >= xpMax)
        {
            Currency.Transaction(CurrencyType.LVL, 1);
            Currency.Transaction(c_xp, -Currency.Balance(c_xp));
            return AddXP(currentXP+xp-xpMax, data);
        }
        else
        {
            Currency.Transaction(c_xp, xp);
            data.lvl_f = Currency.Balance(CurrencyType.LVL);
            data.xp_f_max = xpMax;
            data.xp_f = Currency.Balance(c_xp);
            onXpUpdate?.Invoke(data); 
            return data;
        }
    }

    public static void GetDirectTaskReward(TaskProfile task)
    {
        Currency.Transaction(CurrencyType.CASH, task.cashReward);
        Instance.AddXP(task.xpReward);
    }
#if UNITY_EDITOR
    public CSVDownloadBox csvBox;
    public void LoadFromCSV()
    {
        csvBox.Download(0, (TextAsset text) =>
        {
            CSVReader.Parse_toClassWithBasicConstructor(progressionDataList, text);
            EditorFix.SetObjectDirty(this);
        });
    }

#endif
}
#if UNITY_EDITOR
[CustomEditor(typeof(PlayerProgressManager))]
public class PlayerProfileManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("Load From CSV"))
        {
            (target as PlayerProgressManager).LoadFromCSV();
        }
    }
}
#endif
[System.Serializable]
public class PlayerProgData
{
    public int level;
    public int xpMax;
    public int jobCap;
}

