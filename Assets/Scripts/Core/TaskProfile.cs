﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

[System.Serializable]
public class TaskProfile
{
    public string title;
    public string taskID;
    public int requiredLevel;

    public int cashReward;
    public int xpReward;

    public TaskCategory taskCategory;
    public BuildingType buildingType;
    public int entryCost;

    public string interviewQuestion;
    public string answerRight;
    public string answerWrong;

    private HardData<int> playCountHD;
    
    public int playcount
    {
        get
        {
            if (playCountHD == null)  playCountHD = new HardData<int>(string.Format("playcount_{0}", taskID), 0);
            return playCountHD.value;
        }
    }
    public void IncreasePlayCount()
    {
        if (playCountHD == null) playCountHD = new HardData<int>(string.Format("playcount_{0}", taskID), 0);
        playCountHD.value++;
        AnalyticsAssistant.MinigameStarted(this);
    }

    public bool IsTaskLevelAccessible()
    {
        return requiredLevel <= PlayerProgressManager.currentLevel;
    }
}


public enum TaskCategory
{
    job = 0,
    training = 1,
}
public enum BuildingType
{
    Cafe = 1,
    Factory = 2,
    Gym =3,
    Mall = 4
}
