﻿using System.Collections.Generic;
using UnityEngine;
using FRIA;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class TaskProfileManager : MonoBehaviour
{
    public static TaskProfileManager Instance { get; private set; }

    public List<TaskProfile> allJobs = new List<TaskProfile>();
#if UNITY_EDITOR
    public CSVDownloadBox csvBox;
    public void LoadFromCSV()
    {
        csvBox.Download(0,(TextAsset text)=> 
        {
            CSVReader.Parse_toClassWithBasicConstructor<TaskProfile>(allJobs, text);
            EditorFix.SetObjectDirty(this);
        });
    }

#endif
    private void Awake()
    {
        if (Instance)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    public TaskProfile GetProfile(string taskID)
    {
        foreach (var item in allJobs)
        {
            if (item.taskID == taskID) return item;
        }
        return null;
    }
}
#if UNITY_EDITOR
[CustomEditor(typeof(TaskProfileManager))]
public class JobProfileManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("Load"))
        {
            (target as TaskProfileManager).LoadFromCSV();
        }
    }
}
#endif