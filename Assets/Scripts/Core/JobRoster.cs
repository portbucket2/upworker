﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class JobRoster : MonoBehaviour
{
    public static JobRoster Instance { get; private set; }
    const int MAX_JOB_CAP = 20;

    public static HardData<string> activeJobID;
    public static TaskProfile activeJob
    {
        get 
        {
            return TaskProfileManager.Instance.GetProfile(activeJobID.value);
        }
        set
        {
            if (value == null) 
                activeJobID.value = "";
            else
                activeJobID.value = value.taskID;
        }
    }
    public static List<TaskProfile> visibleTasks;

    //public static HardData<int> jobEntryPointer;
    void Awake()
    {
        if (Instance)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
            Init();
        }
    }
    void Init()
    {
        activeJobID = new HardData<string>("ACTIVE_JOB_0","");
        visibleTasks = new List<TaskProfile>();
        //for (int i = 0; i < MAX_JOB_CAP; i++)
        //{
        //    activeJobs.Add(new HardData<int>(string.Format("ACTIVE_JOB_{0}", i), -1));
        //}
        //jobEntryPointer = new HardData<int>("JOB_ENTRY_POINTER",0);
    }

    //public void AddJobToRoster(int jobID)
    //{
    //    int jobCap = PlayerProgressManager.Instance.GetCurrentProgData().jobCap;
    //    jobEntryPointer.value++;
    //    if (jobEntryPointer.value >= jobCap)
    //    {
    //        jobEntryPointer.value = 0;
    //    }

    //    activeJobs[jobEntryPointer.value].value = jobID;

    //}
    //public void RemoveJobFromRoster(int jobID)
    //{
    //    for (int i = 0; i < MAX_JOB_CAP; i++)
    //    {
    //        if (jobID == activeJobs[i].value)
    //        {
    //            activeJobs[i].value = -1;
    //        }
    //    }
    //}
    //public bool IsJobInRoster(int jobID)
    //{
    //    int jobCap = PlayerProgressManager.Instance.GetCurrentProgData().jobCap;
    //    for (int i = 0; i < jobCap; i++)
    //    {
    //        if (jobID == activeJobs[i].value)
    //        {
    //            return true;
    //        }
    //    }
    //    return false;
    //}

    public Vector2Int jobRange = new Vector2Int(-2,1);
    public Vector2Int trainingRange = new Vector2Int(-5, 1);

    public void UpdateVisibleTaskList()
    {
        int level = PlayerProgressManager.currentLevel;
        visibleTasks.Clear();
        //Debug.Log("AJ"+JobProfileManager.Instance.allJobs.Count);
        //Debug.Log(level);
        foreach (TaskProfile job in TaskProfileManager.Instance.allJobs)
        {
            int min = 0;
            int max = 0;
            switch (job.taskCategory)
            {
                case TaskCategory.job:
                    min = jobRange.x;
                    max = jobRange.y;
                    break;
                case TaskCategory.training:
                    min = trainingRange.x;
                    max = trainingRange.y;
                    break;
            }
            if ((job.requiredLevel >= (level + min) && job.requiredLevel <= (level + max)) || job.taskID==activeJobID.value)
            {
                //Debug.Log("added");
                visibleTasks.Add(job);
            }

        }
    }


    //public TaskProfile GetLatestJob()
    //{
    //    //Debug.Log(jobEntryPointer.value);
    //    int jobid = activeJobs[jobEntryPointer.value].value;
        

    //    foreach (TaskProfile job in visibleTasks)
    //    {
    //        if (job.taskID == jobid)
    //        {
    //            return job;
    //        }
    //    }
    //    return null;

    //}
}
