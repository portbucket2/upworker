﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FRIA;
public class SimpleHarmonicTask : MonoBehaviour
{
    int state_idle = Animator.StringToHash("Idle");
    int state_up = Animator.StringToHash("Up");
    int state_down = Animator.StringToHash("Down");
    int state_complete = Animator.StringToHash("Complete");


    public int maxCounter = 5;

    [Header("References")]
    public Animator animator;

    [Header("Misc Tuning")]
    [SerializeField] float endScreenDelay = 1;


    [Header("Gameplay Tuning")]
    [SerializeField] float initialInputBanTime = 0.5f;
    [SerializeField] float maxSpeed=1;
    [SerializeField] float minSpeed=-0.5f;
    [Range(0,1)]
    [SerializeField] float speedUpMinFactor = 0.55f;
    [SerializeField] float speedDownFactor = 1;


    [Header("Monitor Only")]
    public StateStation<SHState> state = new StateStation<SHState>(SHState.Idle);
    public float upSpeed = 0;
    [SerializeField] int counter;

    bool clearToTakeInput;
    IEnumerator Start()
    {
        InGameUI.SideProgBar.LoadValue(0, 1, true);
        InGameUI.MainProgBar.LoadValue(0, maxCounter, true);

        state.Transition(SHState.Idle, SHState.Up, () => {
            animator.SetTrigger(state_up);
        });


        state.Transition(SHState.Up, SHState.Idle, () => {
            upSpeed = 0;
            animator.SetTrigger(state_idle);
        });
        state.Transition(SHState.Up, SHState.Down, () => {
            animator.SetTrigger(state_down);
        });
        state.Transition(SHState.Up, SHState.Complete, () => {
            animator.SetTrigger(state_complete);
            CamSwapController.SetCamera(CamID.celebration);
            Centralizer.Add_DelayedMonoAct(this, () => {
                EndGameUI.Instance.LoadEndGameUI();
            }, endScreenDelay);
        });


        state.Transition(SHState.Down, SHState.Idle, () => {
            upSpeed = 0;
            animator.SetTrigger(state_idle);
        });


        InGameUI.instructionObject.SetActive(false);
        yield return new WaitForSeconds(initialInputBanTime);
        clearToTakeInput = true;
        InGameUI.instructionObject.SetActive(true);
    }

    float lastTapTime;
    float clipProg;

    void Update()
    {
        if (!clearToTakeInput) return;

        AnimatorStateInfo info = animator.GetCurrentAnimatorStateInfo(0);
        clipProg = animator.GetCurrentAnimatorStateInfo(0).normalizedTime;

        switch (state.currentState)
        {
            case SHState.Idle:
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        state.Set(SHState.Up);
                        upSpeed = 0;
                        Push();
                    }
                }
                break;
            case SHState.Up:
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        if (upSpeed < 0) upSpeed = 0;
                        Push();
                    }
                    else
                    {
                        float oldS = upSpeed;
                        upSpeed = Mathf.Lerp(upSpeed, minSpeed, (Time.time - lastTapTime) * speedDownFactor*Time.deltaTime/Time.fixedDeltaTime);
                        if (clipProg <= 0 && upSpeed < 0)
                        {
                            state.Set(SHState.Idle);
                            break;
                        }
                        else
                        {

                            animator.SetFloat("speed", upSpeed);
                        }
                    }
                    if (clipProg > 1)
                    {
                        counter++;
                        InGameUI.MainProgBar.LoadValue(counter, maxCounter);
                        if (counter < maxCounter)
                        {
                            state.Set(SHState.Down);
                        }
                        else
                        {
                            state.Set(SHState.Complete);
                        }
                        break;
                    }
                }
                break;
            case SHState.Down:
                {
                    if (clipProg > 1)
                    {
                        state.Set(SHState.Idle);
                        break;
                    }
                }
                break;
        }

        if (state.currentState == SHState.Up)
        {

            InGameUI.SideProgBar.LoadValue(clipProg, 1);
        }
        else if (state.currentState != SHState.Complete)
        {
            InGameUI.SideProgBar.LoadValue(0, 1);
        }
    }
    void Push()
    {
        lastTapTime = Time.time;
        float oldS = upSpeed;
        upSpeed = Mathf.Lerp(upSpeed, maxSpeed, speedUpMinFactor + (1 - speedUpMinFactor) * Mathf.Clamp01(1 - clipProg) / 2);
        animator.SetFloat("speed", upSpeed);
    }
}
public enum SHState
{
    Idle = 1,
    Up = 2,
    Down = 3,
    Complete = 4
}