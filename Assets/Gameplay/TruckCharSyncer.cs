﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TruckCharSyncer : MonoBehaviour
{
    public SimpleHarmonicTask shTask;
    public Animator anim;
    StateStation<SHState> stateStation
    {
        get 
        {
            return shTask.state;
        }
    }

    private void Update()
    {

        anim.SetInteger("State", (int)stateStation.currentState);
        switch (stateStation.currentState)
        {

            default:
                break;
            case SHState.Up:
                if (shTask.upSpeed > 0)
                {
                    anim.SetFloat("Speed", shTask.upSpeed);
                }
                else
                {
                    anim.SetFloat("Speed", shTask.upSpeed);
                }
                break;
            //case SHState.Idle:
            //    break;
            //case SHState.Idle:
            //    break;
        }
    }
    //private void Start()
    //{
    //    stateStation.onTransition += OnTransition;
    //}
    //private void OnDestroy()
    //{
    //    stateStation.onTransition -= OnTransition;
    //}

    //SHState mystate;
    //void OnTransition(SHState prev, SHState next)
    //{

    //}

}
