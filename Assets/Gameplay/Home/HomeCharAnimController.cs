﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomeCharAnimController : MonoBehaviour
{
    public Animator anim;

    public int animStyleCount = 2;
    private void Start()
    {
        int choice = Random.Range(0, animStyleCount);

        anim.SetInteger("choice",choice);
    }
}
