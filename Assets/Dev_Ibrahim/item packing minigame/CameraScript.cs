﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    public static List<ItemOverlap> allitems;
    Camera cam;
    public Transform package;
    ItemOverlap selectedItem;
    bool won = false;
    public LayerMask lrm;
    public Transform lid;
    public float lidopenangle = 150;
    float tm;
    public float closingTime = 1;
    // Start is called before the first frame update
    private void Awake()
    {
        allitems = new List<ItemOverlap>();
    }
    void Start()
    {
        cam = GetComponent<Camera>();
        lid.localRotation = Quaternion.Euler(lidopenangle, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit rch;
            if (Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out rch, 500, lrm, QueryTriggerInteraction.Collide))
            {
                selectedItem = rch.rigidbody.GetComponent<ItemOverlap>();
                selectedItem.selected();
            }
        }
        else if (Input.GetMouseButton(0))
        {
            if (selectedItem)
            {
                Vector3 temp = IntersectionPointatPackagepLane();
                selectedItem.transform.position = new Vector3(temp.x, selectedItem.transform.position.y, temp.z);
            }
        }
        else if (Input.GetMouseButtonUp(0) && selectedItem)
        {
            selectedItem.unselected();
            selectedItem = null;
        }
    }
    bool skipAFU;
    void FixedUpdate()
    {
        if (!skipAFU)
        {
            skipAFU = true;
            return;
        }

        if (!won)
        {
            bool b = true;

            foreach (ItemOverlap itov in allitems)
            {
                b = b && itov.inProperPlace();
            }

            if (b)
            {
                Debug.LogFormat("On Game End: {0}",allitems.Count);
                won = true;
                EndGameUI.Instance.LoadEndGameUI();
            }
        }
        else
        {
            tm += Time.deltaTime;

            tm = tm > closingTime ? closingTime : tm;

            lid.localRotation = Quaternion.Euler(Mathf.Lerp(lidopenangle,0,tm/ closingTime), 0, 0);
        }
    }

    Vector3 IntersectionPointatPackagepLane()
    {
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);

        Vector3 localorigin = package.InverseTransformPoint(ray.origin);
        Vector3 localdirection = package.InverseTransformDirection(ray.direction).normalized;
        float slope = -localorigin.y / localdirection.y;
        Vector3 xzintersectionpoint = new Vector3(localorigin.x + slope * localdirection.x, 0, localorigin.z + slope * localdirection.z);

        return package.TransformPoint(xzintersectionpoint);
    }

    
}
