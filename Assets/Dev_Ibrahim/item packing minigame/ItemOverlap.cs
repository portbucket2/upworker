﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemOverlap : MonoBehaviour
{
    public bool overlapping;
    bool onedge;
    public bool overlapstarted;
    public bool overlapended;
    public MeshRenderer highlightmesh;
    public float overlappingcooldown = 0.5f;
    public float cooldown;
    public int boundindex;
    bool IsItemSelected;
    Vector3 positionstore;
    // Start is called before the first frame update
    void Start()
    {
        CameraScript.allitems.Add(this);
        positionstore = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (overlapping)
        {
            if (!overlapstarted)
            {
                //1st overlapping frame
                OverlapItemStarted();
                overlapstarted = true;
                overlapended = false;
            }

            overlappingItem();

            //cooldown = 0;
        }
        else
        {
            if (IsItemSelected)
            {
                bool b = boundindex > 0;

                highlightmesh.material.SetColor("Color_2E0EB390", b ? new Color(1, 0, 0, 0.5f) : new Color(0, 1, 0, 0.5f));
            }
        }

        if (cooldown < overlappingcooldown)
        {
            cooldown += Time.deltaTime;

        }
        else
        {
            overlapping = false;

            if (!overlapended)
            {
                //1st non overlapping frame
                OverlapItemEnded();
                overlapstarted = false;
                overlapended = true;
            }
        }
    }

    void OverlapItemStarted()
    {
        highlightmesh.material.SetColor("Color_2E0EB390", new Color(1, 0, 0, 0.5f));
    }

    void OverlapItemEnded()
    {
        highlightmesh.material.SetColor("Color_2E0EB390", new Color(0, 0, 0, 0));

        
    }

    public void selected()
    {
        IsItemSelected = true;
    }

    public void unselected()
    {
        IsItemSelected = false;
        highlightmesh.material.SetColor("Color_2E0EB390", new Color(0, 0, 0, 0));
        if (overlapping || boundindex > 0 )
        {
            transform.position = positionstore;
        }
        else
        {
            positionstore = transform.position;
        }
    }

    public bool inProperPlace()
    {
        return boundindex <= 0 && !overlapping && !IsItemSelected;
    }

    void overlappingItem()
    {

    }
    private void OnTriggerStay(Collider other)
    {
        if (other.attachedRigidbody)
        {
            if (other.attachedRigidbody.GetComponent<ItemOverlap>())
            {
                overlapping = true;
                cooldown = 0;
            }
        }
        

        //Debug.Log("colliding");
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("OutofBounds"))
        {
            boundindex++;
        }

        //if (!other.attachedRigidbody)
        //{
        //    boundindex++;
        //}
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("OutofBounds"))
        {
            boundindex--;
        }

        //if (!other.attachedRigidbody)
        //{
        //    boundindex++;
        //}
    }
}
