﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIController_Factory : MonoBehaviour
{
    [SerializeField] Button tapToFillButton;
    [SerializeField] Animator accuracyMeter;
    [SerializeField] Transform waypoint1;
    //[SerializeField] BarUI progressionBar;
    [SerializeField] GameObject endPanel;

    GameController_Factory gameController_Factory;
    Transform fillPostion;
    float totaldistance = 0;
    Transform currentBox;
    readonly string BLEND = "Blend";
    bool isCalculating = false;

    void Start()
    {
        gameController_Factory = GameController_Factory.GetController();
        ButtonInit();
        fillPostion = gameController_Factory.GetFillPosition();
        totaldistance = Vector3.Distance(waypoint1.position, fillPostion.position);
    }
    private void Update()
    {
        if (isCalculating)
        {
            AccuracyMeter();
        }
    }

    void ButtonInit()
    {
        tapToFillButton.onClick.AddListener(delegate
        {
            gameController_Factory.ButtonFillBox();
        });
    }
    public void ProgressionBarUpdate(int count, int max)
    {
        InGameUI.MainProgBar.LoadValue(count, max);
    }
    public void AccuracyMeterStart(Transform _currentBox)
    {
        currentBox = _currentBox;
        isCalculating = true;
    }
    public void ShowEndPanel()
    {
        endPanel.SetActive(true);
    }
    void AccuracyMeter()
    {
        float distance = Vector3.Distance(currentBox.position, fillPostion.position);
        float dd = distance / totaldistance;

        if (dd <=0)
            dd = 0f;
        else if (dd >= 1)
            dd = 1f;

        float blend = Mathf.Lerp(accuracyMeter.GetFloat(BLEND), dd, Time.deltaTime * 10f);
        
        accuracyMeter.SetFloat(BLEND, blend);

    }
}
