﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class GameController_Factory : MonoBehaviour
{
    public static GameController_Factory gameController;

    [SerializeField] int maximumBoxes;
    [SerializeField] GameObject boxPrefab;
    [SerializeField] GameObject packagePrefab;
    [SerializeField] Transform packagePosition;
    [SerializeField] Transform fillPosition;
    [SerializeField] Transform secondCraneHolder;
    [SerializeField] Animator craneOperator;
    [SerializeField] Animator craneOperator2;
    [SerializeField] UIController_Factory uiController;
    [SerializeField] LineRenderer ln1;
    [SerializeField] LineRenderer ln2;
    [SerializeField] float greenZone = 1f;
    [SerializeField] List<Transform> waypointObjects;
    [SerializeField] Text titleText;
    [Header("for Debug only : ")]
    [SerializeField] List<Vector3> waypoints;
    [SerializeField] BoxController currentBox;
    [SerializeField] int totalBoxFilled = 0;
    [SerializeField] bool isInputEnabled = true;
    [SerializeField] bool inProduction = true;

    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    WaitForSeconds WAITHALF = new WaitForSeconds(0.5f);
    WaitForSeconds WAITONE = new WaitForSeconds(1f);
    WaitForSeconds WAITTWO = new WaitForSeconds(2f);
    WaitForSeconds WAITTHREE = new WaitForSeconds(3f);

    Vector3 spwanOffset = new Vector3(0f, 0f, 1f);
    

    readonly string RELEASE = "release";
    readonly string RELOAD = "reload";
    readonly string FAIL = "fail";
    readonly string END = "end";

    private void Awake()
    {
        gameController = this;
    }

    public static GameController_Factory GetController()
    {
        return gameController;
    }

    void Start()
    {
        if (titleText && SceneData.runningGameProfile != null) titleText.text = SceneData.runningGameProfile.title;
        int max = waypointObjects.Count;
        waypoints = new List<Vector3>();
        for (int i = 0; i < max; i++)
        {
            waypoints.Add(waypointObjects[i].position);
        }
        //==================================================This starts the level, need be set before level start
        SetMaximumBoxes(maximumBoxes);
        //--------------------------------------
        ShowLimitLines();
        uiController.ProgressionBarUpdate(totalBoxFilled, maximumBoxes);
    }

    public void SetMaximumBoxes(int max)
    {
        maximumBoxes = max;
        FactoryStart();
    }

    void FactoryStart()
    {
        if (inProduction)
        {
            isInputEnabled = true;
            StartCoroutine(FactoryRoutine());
        }
        
    }

    IEnumerator FactoryRoutine()
    {

        //GameObject gg = Instantiate(boxPrefab, boxPrefab.transform.position + spwanOffset, boxPrefab.transform.rotation);
        GameObject gg = Instantiate(boxPrefab, secondCraneHolder.position, boxPrefab.transform.rotation);
        gg.transform.SetParent(secondCraneHolder);
        BoxController bc = gg.GetComponent<BoxController>();
        bc.Setup(waypoints);
        currentBox = bc;
        uiController.AccuracyMeterStart(currentBox.transform);
        craneOperator2.SetTrigger(RELOAD);
        yield return WAITHALF;
        bc.transform.parent = null;
        bc.GetComponent<Rigidbody>().isKinematic = false;
        yield return WAITHALF;
        bc.GetComponent<Rigidbody>().isKinematic = true;
        bc.FirstMove();
        bc.transform.DOLocalRotate(Vector3.zero,0.2f);
        yield return WAITTWO;
        FactoryStart();
    }
    void FactoryEnd()
    {
        // end of level
        
        StartCoroutine(FactoryEndRoutine());
    }

    public void ButtonFillBox()
    {
        if (isInputEnabled)
        {
            float distance = Vector3.Distance(currentBox.transform.position, fillPosition.position);
            if (distance < greenZone)
            {
                //fill the box            
                currentBox.BoxFilledSuccess();
                totalBoxFilled++;
                //Debug.Log("filling: " + distance+" / total: "+ totalBoxFilled);
                craneOperator.SetTrigger(RELEASE);
                LevelEndCheck();
            }
            else
            {
                // fill box fail
                Debug.Log("filling failed: " + distance);
                craneOperator.SetTrigger(FAIL);
                currentBox.BoxFilledFail();
                
                GameObject pp = Instantiate(packagePrefab, packagePrefab.transform.position , packagePrefab.transform.rotation);
            }
            isInputEnabled = false;
        }
    }
    public Transform GetFillPosition()
    {
        return fillPosition;
    }
    void LevelEndCheck()
    {
        ProgressCheck();
        if (totalBoxFilled >= maximumBoxes)
        {
            FactoryEnd();
        }
    }
    void ProgressCheck()
    {
        //float progress = (float)totalBoxFilled / maximumBoxes;
        //if (totalBoxFilled == 0)
        //{
        //    progress = 0;
        //}
        //Debug.Log("progress check: " + progress);
        uiController.ProgressionBarUpdate(totalBoxFilled,maximumBoxes);
    }
    void ShowLimitLines()
    {
        ln1.SetPosition(0, new Vector3(fillPosition.position.x - greenZone, fillPosition.position.y + 0.2f, 2.78f));
        ln1.SetPosition(1, new Vector3(fillPosition.position.x + 0.2f, fillPosition.position.y + 0.2f, 2.78f));

        ln2.SetPosition(0, new Vector3(fillPosition.position.x - 5f, fillPosition.position.y + 0.2f, 2.78f));
        ln2.SetPosition(1, new Vector3(fillPosition.position.x , fillPosition.position.y + 0.2f, 2.78f));
    }
    IEnumerator FactoryEndRoutine()
    {
        
        //Debug.Log("level end");
        inProduction = false;
        yield return WAITONE;
        craneOperator.SetTrigger(END);
        EndGameUI.Instance.LoadEndGameUI();
        //uiController.ShowEndPanel();
    }
}
