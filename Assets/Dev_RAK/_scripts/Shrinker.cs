﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shrinker : MonoBehaviour
{
    // Start is called before the first frame update
    WaitForEndOfFrame END = new WaitForEndOfFrame();
    WaitForSeconds WAIT = new WaitForSeconds(3f);

    Vector3 rate = new Vector3(0.01f, 0.01f, 0.01f);

    void Start()
    {
        StartCoroutine(Shrink());
    }

    IEnumerator Shrink()
    {
        yield return WAIT;
        while (transform.localScale.x > 0.02f)
        {
            transform.localScale -= rate;
            yield return END;
        }
        //Debug.LogError("Shrink complete");
    }
}
