﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public class BoxController : MonoBehaviour
{

    [SerializeField] float duration = 1f;
    [Header ("for debug:")]
    [SerializeField] List<Vector3> waypoints;
    [SerializeField] int waypointCount = 0;

    Animator animator;
    Rigidbody rb;

    int totalWaypoints = 0;

    readonly string OPEN = "open";
    readonly string CLOSE = "close";

    void Start()
    {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 8)
        {
            //Debug.LogError("package!!!");
            collision.transform.GetComponent<Rigidbody>().AddForce(Vector3.right *2f);
        }
    }

    public void Setup(List<Vector3> _waypoints)
    {
        waypoints = new List<Vector3>();
        waypoints = _waypoints;
        waypointCount = 0;
        totalWaypoints = waypoints.Count;
        //Move();
    }
    public void BoxFilledSuccess()
    {
        animator.SetTrigger(CLOSE);
    }
    public void BoxFilledFail()
    {
        rb.isKinematic = false;
    }
    public void Reset()
    {
        animator.SetTrigger(OPEN);
    }
    public void FirstMove()
    {
        transform.DOLocalMove(waypoints[waypointCount], 0.1f).SetEase(Ease.Flash).OnComplete(() => { waypointCount++; Move(); }) ;
    }
    void Move()
    {
        transform.DOLocalMove(waypoints[waypointCount], duration).SetEase(Ease.Linear).OnComplete(OnWayPointComplete);        
    }
    void OnWayPointComplete()
    {
        waypointCount++;
        if (waypointCount >= totalWaypoints)
        {
            EndOfWaypoint();
        }else
            Move();
    }

    void EndOfWaypoint()
    {
        gameObject.SetActive(false);
    }

   

}
